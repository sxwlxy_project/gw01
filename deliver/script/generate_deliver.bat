::/**
:: * @addtogroup generate_deliver
:: * generate_deliver
:: * @{
:: */
::/**
:: * @file        generate_deliver.bat
:: * @brief       XXXX
:: * @note        XXXX
:: * @author      靳普诏
:: * @date        2024/04/13
:: * @version     1.0
::
:: * @par         修改日志
:: * <table>
:: * <tr><th>Date         <th>Version     <th>Author      <th> Description
:: * <tr><td>2024/04/13   <td>1.0         <td>靳普诏       <td> 创建初始版本
:: * @copyright   xxxx
:: */

@echo off
:: ============================= 获取环境参数 ================================

set ARGV_LEN=%#
::echo "There are %ARGV_LEN% arguments passed to the generate_deliver.bat"

set i=1
:show_argument
if %i% gtr %ARGV_LEN% goto end_show_argument

set arg=!%i%!
::echo "Argument %i%: %arg%"

set /a i+=1
goto show_argument

:end_show_argument
::echo "All arguments printed"

:: 脚本所在路径
set SCRIPT_DIR=%~dp0
echo "SCRIPT_DIR=%SCRIPT_DIR%"
:: 当前工作路径
set CURRENT_DIR=%cd%
echo "CURRENT_DIR=%CURRENT_DIR%"    

:: ============================= 实际调用逻辑 ================================
echo "============= BIGEN: generate_deliver.bat ============="

set DELIVER_PACKAGE_DIR=%SCRIPT_DIR%/../deliver_package
echo "DELIVER_PACKAGE_DIR=%DELIVER_PACKAGE_DIR%"

set SOURCE_DIR=%SCRIPT_DIR%/../../software_project/


@echo on
:: 复制指定目录
xcopy "%SOURCE_DIR%/app" "%DELIVER_PACKAGE_DIR%/app" /s /i
xcopy "%SOURCE_DIR%/bsp/output" "%DELIVER_PACKAGE_DIR%/bsp/output" /s /i
xcopy "%SOURCE_DIR%/bsp/refer" "%DELIVER_PACKAGE_DIR%/bsp/refer" /s /i
xcopy "%SOURCE_DIR%/package/abc/common/include" "%DELIVER_PACKAGE_DIR%/package/abc/common/include" /s /i
xcopy "%SOURCE_DIR%/package/bot/common/include" "%DELIVER_PACKAGE_DIR%/package/bot/common/include" /s /i
xcopy "%SOURCE_DIR%/package/mdw/common/include" "%DELIVER_PACKAGE_DIR%/package/mdw/common/include" /s /i
xcopy "%SOURCE_DIR%/package/osc/common/include" "%DELIVER_PACKAGE_DIR%/package/osc/common/include" /s /i
xcopy "%SOURCE_DIR%/package/osc/build/stm32f103vet6/project/Objects" "%DELIVER_PACKAGE_DIR%/package/osc/build/stm32f103vet6/project/Objects"  /s /i
xcopy "%SOURCE_DIR%/project/gw01_demo" "%DELIVER_PACKAGE_DIR%/project/gw01_demo" /s /i
xcopy "%SOURCE_DIR%/project/gw01_lib/Objects" "%DELIVER_PACKAGE_DIR%/project/gw01_lib/Objects" /s /i
xcopy "%SOURCE_DIR%/rte" "%DELIVER_PACKAGE_DIR%/rte" /s /i
@echo off

echo "============= END: generate_deliver.bat ============="
echo "generate_deliver.bat return errorlevel:%errorlevel%"

pause
:: Generated on "2024-04-13 13:00:39" by the tool "gen_hq_file.py >> V20231119" 


