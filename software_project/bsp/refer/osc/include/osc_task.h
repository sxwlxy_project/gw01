/**
 * @File    : Osc_task.h
 * @Brief   : 线程(任务)对象
 * @Version : 1.0
 * @Author  : 靳普诏
 * @E-Mail  : 499367619@qq.com
 */


#ifndef _OSC_TASK_H_
#define _OSC_TASK_H_

#include "osc_api.h"

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * 线程优先级的枚举
 */
typedef enum Osc_TTaskPriorityTag Osc_TTaskPriority;
enum Osc_TTaskPriorityTag
{
    Osc_kTaskPriorityLow = 0,
    Osc_kTaskPriorityBelowNormal = 1,
    Osc_kTaskPriorityNormal = 2,
    Osc_kTaskPriorityNormalPlus = 3,
    Osc_kTaskPriorityAboveNormal = 4,
    Osc_kTaskPriorityHigh = 5,
    Osc_kTaskPriorityRealtime = 6,
};



typedef struct Osc_TTaskTag Osc_TTask;


/**
 * 任务状态切换回调
 * @param *self 对象句柄
 * @return 
 */
typedef void (*Osc_TTaskOnStateChangeMethod)(Osc_TTask *self);


/**
 * 任务运行函数
 * @param *self   对象句柄
 * @return 返回运行一次后等待时间。单位，ms
 */
typedef UInt32 (*Osc_TTaskOnRunOnceMethod)(Osc_TTask *self);



struct Osc_TTaskTag
{
    void *handle_;
    
    Osc_TTaskOnStateChangeMethod OnStart;
    Osc_TTaskOnRunOnceMethod OnRunOnce;
    Osc_TTaskOnStateChangeMethod OnTerminate;
};


typedef struct Osc_TTaskAttrTag Osc_TTaskAttr;
struct Osc_TTaskAttrTag
{
    const char *name;
    Int8 *stack_mem;
    UInt16 stack_size;
    UInt8 priority;
};



/**
 * 任务创建
 * @param *self         任务句柄
 * @param *attr         任务属性
 * @param run_method    任务方法
 * @return >= 0 成功
 *         <  0 失败
 */
Int32 Osc_TTaskCreate(Osc_TTask *self, const Osc_TTaskAttr *attr);


/**
 * 任务销毁
 * @param *self         任务句柄
 * @return 
 */
void Osc_TTaskDestroy(Osc_TTask *self);


/**
 * 开始任务
 * @param *self
 * @return 
 */
Int32 Osc_TTaskStart(Osc_TTask *self);

/**
 * 任务正在执行
 * @param *self
 * @return 
 */
Bool Osc_TTaskIsTerminated(Osc_TTask *self);


/**
 * 停止任务
 * @param *self
 * @return 
 */
void Osc_TTaskTerminate(Osc_TTask *self);



#ifdef __cplusplus
} ///< extern "C"
#endif


#endif ///< _OSC_TASK_H_
