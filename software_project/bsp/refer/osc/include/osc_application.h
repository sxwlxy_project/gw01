/**
 * @addtogroup Osc_application
 * Osc_application
 * @{
 */
/**
 * @file        Osc_application.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/05
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/05   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */

#ifndef _Osc_APPLICATION_H_
#define _Osc_APPLICATION_H_

#include "osc_task.h"

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct Osc_TApplication Osc_TApplication;

/**
 * 应用框架
 */
struct Osc_TApplication
{
    Osc_TTask *task_;                                                   /**< 主任务 */
};


/**
 * 应用框架初始化
 * @note 主任务必须在调用完这个函数后创建
 * @param self: 对象实例
 * @return 无返回值
 */
void Osc_TApplicationInitialize(Osc_TApplication *self);

/**
 * 获取应用框架的主任务
 * @param self: 对象实例
 * @return 返回指向主任务的对象
 */
Osc_TTask *Osc_TApplicationMainTask(Osc_TApplication *self);

/**
 * 设置应用框架的主任务.
 * @note 这个函数必须在[Osc_TApplicationInitialize](@ref Osc_TApplicationInitialize)之后,[Osc_TApplicationRun](@ref Osc_TApplicationRun)前调用
 * @param self: 对象实例
 * @param task: 创建的主任务 
 * @return 无返回值
 */
void Osc_TApplicationSetMainTask(Osc_TApplication *self, Osc_TTask *task);

/**
 * 应用框架运行
 * @note 调用前需要创建一个主任务
 * @param self: 对象实例
 * @return 无返回值
 */
void Osc_TApplicationRun(Osc_TApplication *self);

/// 主应用框架对象.整个应用程序仅此一份
extern Osc_TApplication g_osc_application;




#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _Osc_APPLICATION_H_

/**
 * @}  Generated on "2024-04-05 23:35:20" by the tool "gen_hq_file.py >> V20231119" 
 */

