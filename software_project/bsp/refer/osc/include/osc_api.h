/**
 * @File    : Osc_api.h
 * @Brief   : 操作系统通用命令
 * @Version : 1.0
 * @Author  : 靳普诏
 * @E-Mail  : 499367619@qq.com
 */

#ifndef _OSC_API_H_
#define _OSC_API_H_

#include "abc_types.h"

#ifdef __cplusplus
extern "C" 
{
#endif


/**
 * 获取操作系统信息
 * @param ret_info 返回相关字符串信息
 * @return 关于版本整型值
 */
Int32 Osc_Version(PChar ret_info);


/**
 * 获取操作系统当前Tick值
 * @return 当前Tick值
 */
UInt32 Osc_SystemTick(void);


/**
 * 睡眠 毫秒ms
 * @param ms
 * @return 
 */
Bool Osc_SleepMs(UInt32 ms);


///**
// * 睡眠 微秒us
// * @param us
// * @return 
// */
//void Osc_SleepUs(UInt32 us);




#ifdef __cplusplus
} ///< extern "C"
#endif



#endif ///< _OSC_API_H_
