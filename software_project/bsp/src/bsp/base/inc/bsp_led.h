/**
 * @file bsp_led.h
 * @brief 板级支持包-LED灯接口
 * @author 彭泽稳
 * @version v1.0
 * @par 修改历史
 * 版本     修改日期        修改人      修改内容
 * v1.0     2024-04-18      彭泽稳      第一版
 * @par 实例
 * Bsp_Init();
 * Bsp_Open();
 * 
 * Bsp_LedSetOn(0, True);   ///< 点亮LED0
 * Bsp_LedSetOn(0, False);  ///< 熄灭LED0
 * 
 * Bool status;
 * Bsp_LedGetStatus(0, &status);    ///< 获取LED0状态
 * 
 * Bsp_Close();
 * Bsp_Done();
 * 
 * @copyright Copyright (c) 2024, 彭泽稳
*/
#ifndef _BSP_LED_H_
#define _BSP_LED_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "bsp_base.h"

/**
 * @brief: 设置LED亮灭
 * @param id: [in], LED标识
 * @param status: [in], True: 点亮LED， False: 熄灭LED
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
*/
Int32 Bsp_LedSetOn(Int32 id, Bool status);

/**
 * @brief: 读取LED当前状态
 * @param id: [in], LED标识
 * @param status: [out], LED状态, True: 点亮, False: 熄灭
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
*/
Int32 Bsp_LedGetStatus(Int32 id, Bool* status);

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif // _BSP_LED_H_
/**
 * @} 
 */
