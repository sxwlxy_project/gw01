/**
 * @file bsp_motor.h
 * @brief 板级支持包-电机接口
 * @author 彭泽稳
 * @version v1.0
 * @par 修改历史
 * 版本     修改日期        修改人      修改内容
 * v1.0     2024-04-18      彭泽稳      第一版
 * @par 实例
 * Bsp_Init();
 * Bsp_Open();
 * 
 * Bsp_THandle handle = Bsp_MotorCreate(0);
 * 
 * /// 打开
 * Bsp_MotorOpen(handle);
 * 
 * Bsp_MotorSetDirection(handle, True); ///< 前进
 * Bsp_MotorSetSpeed(handle, 50);   ///< 设置速度为500%
 * 
 * Bsp_MotorStart(handle);  ///< 启动
 * Delay(1000);
 * Bsp_MotorStop(handle);   ///< 停止
 * 
 * /// 关闭
 * Bsp_MotorClose(handle);
 * 
 * Bsp_MotorFree(handle);
 * 
 * Bsp_Close();
 * Bsp_Done();
 * 
 * @copyright Copyright (c) 2024, 彭泽稳
*/
#ifndef _BSP_MOTOR_H_
#define _BSP_MOTOR_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "bsp_base.h"

/**
 * 创建电机对象.
 * @param id: [in], 电机标识
 * @return 如果创建失败返回NULL
 */
Bsp_THandle Bsp_MotorCreate(Int32 id);

/**
 * 释放电机对象.
 * @param handle: [in], 句柄
 * @return 
 */
void Bsp_MotorFree(Bsp_THandle handle);

/**
 * 获取电机标识
 * @param handle: [in], 句柄
 * @return 返回用于创建句柄的标识, 参见Bsp_TMotorId(@ bsp_motor_def.h)
 */
Int32 Bsp_MotorGetId(Bsp_THandle handle);

/**
 * 打开设备
 * @param handle: [in], 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_MotorOpen(Bsp_THandle handle);

/**
 * 关闭设备
 * @param handle: 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_MotorClose(Bsp_THandle handle);

/**
 * 返回当前电机是否已经打开
 * @param handle: [in], 句柄
 * @return 返回 True, 为已打开; 返回 False, 为未打开
 */
Bool Bsp_MotorIsOpen(Bsp_THandle handle);

/**
 * 设置电机运行方向
 * @param handle: [in], 句柄
 * @param direction: [in], True: 前进, False: 后退
 * @return 当返回值 >= 0 时, 表示成功
 *         当返回值 < 0 时, 表示失败参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_MotorSetDirection(Bsp_THandle handle, Bool direction);

/**
 * 获取电机运行方向
 * @param handle: [in], 句柄
 * @param direction: [out], True: 前进, False: 后退
 * @return 当返回值 >= 0 时, 表示成功
 *         当返回值 < 0 时, 表示失败参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_MotorGetDirection(Bsp_THandle handle, Bool* direction);

/**
 * 设置电机运行速度
 * @param handle: [in], 句柄
 * @param speed: [in], 速度百分比, 100表示100%, 0表示0% ......
 * @return 当返回值 >= 0 时, 表示成功
 *         当返回值 < 0 时, 表示失败参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_MotorSetSpeed(Bsp_THandle handle, Int32 speed);

/**
 * 获取电机运行速度
 * @param handle: [in], 句柄
 * @param speed: [out], 速度百分比, 100表示100%, 0表示0% ......
 * @return 当返回值 >= 0 时, 表示成功
 *         当返回值 < 0 时, 表示失败参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_MotorGetSpeed(Bsp_THandle handle, Int32* speed);

/**
 * 设置电机调速PWM的频率和占空比
 * @note 默认使用内部设置的频率和占空比, 当你有特殊需求时, 使用此函数设置PWM的频率和占空比
 *       当使用完此函数后，你仍可以使用 Bsp_MotorSetDirection 和 Bsp_MotorSetSpeed
 * @param handle: [in], 句柄
 * @param freq: [in], PWM频率
 * @param duty: [in], PWM占空比
 * @return 当返回值 >= 0 时, 表示成功
 *         当返回值 < 0 时, 表示失败参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_MotorSetFreqDuty(Bsp_THandle handle, Int32 freq, Int32 duty);

/**
 * 启动电机
 * @param handle: [in], 句柄
 * @return 当返回值 >= 0 时, 表示成功
 *         当返回值 < 0 时, 表示失败参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_MotorStart(Bsp_THandle handle);

/**
 * 停止电机
 * @param handle: [in], 句柄
 * @return 当返回值 >= 0 时, 表示成功
 *         当返回值 < 0 时, 表示失败参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_MotorStop(Bsp_THandle handle);

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif // _BSP_MOTOR_H_
/**
 * @} 
 */
