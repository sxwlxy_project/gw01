/**
 * @file bsp_base.h
 * @brief 板级支持包基础接口
 * @author 彭泽稳
 * @version v1.0
 * @par 修改历史
 * 版本     修改日期        修改人      修改内容
 * v1.0     2024-04-17      彭泽稳      第一版
 * @par 实例
 * Bsp_Init();
 * Bsp_Open();
 * 
 * Bsp_TVersion version;
 * Bsp_GetVersion(&version);
 * 
 * Bsp_Close();
 * Bsp_Done();
 * 
 * @copyright Copyright (c) 2024, 彭泽稳
*/
#ifndef _BSP_BASE_H_
#define _BSP_BASE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "bsp_error.h"
#include <BaseTypes.h>

/// BSP版本定义
typedef struct Bsp_TVersionTag Bsp_TVersion;
struct Bsp_TVersionTag
{
    UInt8 ver[4];           ///< 版本号
    UInt32 build_time;      ///< 编译时间
    const void* desc;       ///< 描述信息
};

/// BSP句柄定义
typedef void * Bsp_THandle;

/**
 * @brief: BSP初始化
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
*/
Int32 Bsp_Init(void);

/**
 * @brief: BSP打开
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
*/
Int32 Bsp_Open(void);

/**
 * @brief: BSP关闭
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
*/
Int32 Bsp_Close(void);

/**
 * @brief: BSP去初始化
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
*/
Int32 Bsp_Done(void);

/**
 * @brief: 获取BSP版本号
 * @param version: [out] 获取BSP版本号
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
*/
Int32 Bsp_GetVersion(Bsp_TVersion* version);

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_BASE_H_ */
