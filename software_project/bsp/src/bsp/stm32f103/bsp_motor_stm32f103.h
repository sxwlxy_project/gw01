#ifndef _BSP_MOTOR_STM32F103_H_
#define _BSP_MOTOR_STM32F103_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "bsp_base.h"
#include <BaseTypes.h>

/// 电机项
typedef struct Bsp_TMotorItemTag Bsp_TMotorItem;
struct Bsp_TMotorItemTag
{
    const Int32 id;             ///< 标识
    Int32 freq;                 ///< 频率
    Int32 duty;                 ///< 占空比
    Bool direction;             ///< 方向, True: 正向, False: 反向
    Bool is_open;               ///< 打开标志
};

/// 电机集合
typedef struct Bsp_TMotorUnionTag Bsp_TMotorUnion;
struct Bsp_TMotorUnionTag
{
    Int32 count;
    Bsp_TMotorItem* items;
};

/// 外部定义全局motor集合对象
extern Bsp_TMotorUnion g_bsp_motor_union;

void Bsp_MotorInit_(void);

void Bsp_MotorDone_(void);

void Bsp_MotorOpen_(void);

void Bsp_MotorClose_(void);

void Drv_MotorOpen(void);

void Drv_MotorClose(void);

void Drv_MotorSetDirection(Bsp_TMotorItem* item);

void Drv_MotorSetDuty(Bsp_TMotorItem* item);

void Drv_MotorStart(Bsp_TMotorItem* item);

void Drv_MotorStop(Bsp_TMotorItem* item);

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_MOTOR_STM32F103_H_ */
