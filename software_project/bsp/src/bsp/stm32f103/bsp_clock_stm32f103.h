#ifndef _BSP_CLOCK_STM32F103_H_
#define _BSP_CLOCK_STM32F103_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>

void Bsp_CLockInit_(void);

void Bsp_CLockDone_(void);

void Bsp_CLockOpen_(void);

void Bsp_CLockClose_(void);

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_CLOCK_STM32F103_H_ */
