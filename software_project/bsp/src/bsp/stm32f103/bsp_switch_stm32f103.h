#ifndef _BSP_SWITCH_STM32F103_H_
#define _BSP_SWITCH_STM32F103_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>
#include "stm32f1xx_ll_gpio.h"

/// 开关量配置
typedef struct Bsp_TSwitchCfgTag Bsp_TSwitchCfg;
struct Bsp_TSwitchCfgTag
{
    GPIO_TypeDef* port;             ///< 端口
    UInt32 pin;                     ///< 引脚
};

/// 开关量项
typedef struct Bsp_TSwitchItemTag Bsp_TSwitchItem;
struct Bsp_TSwitchItemTag
{
    UInt8 id;                       ///< 标识 
    const Bsp_TSwitchCfg* cfg;      ///< 开关量配置
};

/// 开关量集合
typedef struct Bsp_TSwitchUnionTag Bsp_TSwitchUnion;
struct Bsp_TSwitchUnionTag
{
    Int32 count;
    Bsp_TSwitchItem* items;
};

/// 外部定义全局开关量集合对象
extern Bsp_TSwitchUnion g_bsp_switch_union;

void Bsp_SwitchInit_(void);

void Bsp_SwitchDone_(void);

void Bsp_SwitchOpen_(void);

void Bsp_SwitchClose_(void);

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_SWITCH_STM32F103_H_ */
