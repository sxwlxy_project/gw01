#ifndef _BSP_LASER_STM32F103_H_
#define _BSP_LASER_STM32F103_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "BaseTypes.h"
#include "ofc_ring_buf.h"
#include "bsp_laser_def.h"
#include "bsp_laser.h"

typedef struct Bsp_TLaserAttrTag Bsp_TLaserAttr;

/// 硬件读取数据的回调函数
/// >= 0返回实际读到的字节个数, < 0表示读取失败
typedef int (* Bsp_TLaserOnReadBytes)(Bsp_TLaserAttr* self, void* buf, int size);

/// 硬件发送数据的回调函数
/// >= 0返回实际发送的字节个数, < 0表示发送失败
typedef int (* Bsp_TLaserOnWriteBytes)(Bsp_TLaserAttr* self, void* data, int size);

/// 激光传感器属性
struct Bsp_TLaserAttrTag
{
    Bsp_TLaserOnReadBytes on_read;          ///< 读回调
    Bsp_TLaserOnWriteBytes on_write;        ///< 写回调
    void* user_data;                        ///< 用户数据

    UInt8 laser_id;                         ///< 激光传感器标识
    Bsp_THandle laser_handle;               ///< 激光传感器驱动句柄
    void* recv_buf;                         ///< 接收缓冲区
    void* send_buf;                         ///< 发送缓冲区
    Int32 recv_buf_size;                    ///< 接收缓冲区大小
    Int32 send_buf_size;                    ///< 发送缓冲区大小

    void* read_buf;                         ///< 读缓冲区
    Int32 read_buf_size;                    ///< 读缓冲区大小

    Bsp_TLaserData* data;                   ///< 数据
    Int32 depth;                            ///< 数据深度
    TOfRingBuf ring_buf;                    ///< 数据环形缓冲区

    Bool is_open;                           ///< 打开标志
};

extern Bsp_TLaserAttr g_bsp_laser_attr;

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif /** _BSP_LASER_STM32F103_H_ */
