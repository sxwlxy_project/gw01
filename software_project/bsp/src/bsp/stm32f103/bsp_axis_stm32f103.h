#ifndef _BSP_AXIS_STM32F103_H_
#define _BSP_AXIS_STM32F103_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "BaseTypes.h"
#include "ofc_ring_buf.h"
#include "bsp_axis_def.h"
#include "bsp_axis.h"

typedef struct Bsp_TAxisAttrTag Bsp_TAxisAttr;

/// 硬件读取数据的回调函数
/// >= 0返回实际读到的字节个数, < 0表示读取失败
typedef int (* Bsp_TAxisOnReadBytes)(Bsp_TAxisAttr* self, void* buf, int size);

/// 硬件发送数据的回调函数
/// >= 0返回实际发送的字节个数, < 0表示发送失败
typedef int (* Bsp_TAxisOnWriteBytes)(Bsp_TAxisAttr* self, void* data, int size);

/// 轴属性
struct Bsp_TAxisAttrTag
{
    Bsp_TAxisOnReadBytes on_read;           ///< 读回调
    Bsp_TAxisOnWriteBytes on_write;         ///< 写回调
    void* user_data;                        ///< 用户数据

    UInt8 axis_id;                          ///< 轴标识
    Bsp_THandle axis_handle;                ///< 轴驱动句柄
    void* recv_buf;                         ///< 读缓冲区
    void* send_buf;                         ///< 发送缓冲区
    Int32 recv_buf_size;                    ///< 读缓冲区大小
    Int32 send_buf_size;                    ///< 发送缓冲区大小

    void* read_buf;                         ///< 读缓冲区
    Int32 read_buf_size;                    ///< 读缓冲区大小

    Bsp_TAxisAcceData* acce_data;           ///< 加速度数据
    Int32 acce_depth;                       ///< 加速度数据深度
    Bsp_TAxisGyroData* gyro_data;           ///< 角速度数据
    Int32 gyro_depth;                       ///< 角速度数据深度
    Bsp_TAxisAngleData* angle_data;         ///< 角度数据
    Int32 angle_depth;                      ///< 角度数据深度
    TOfRingBuf acce_ring_buf;               ///< 加速度数据环形缓冲区
    TOfRingBuf gyro_ring_buf;               ///< 角速度数据环形缓冲区
    TOfRingBuf angle_ring_buf;              ///< 角度数据环形缓冲区

    Bool is_open;                           ///< 打开标志
};

extern Bsp_TAxisAttr g_bsp_axis_attr;

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif /** _BSP_AXIS_STM32F103_H_ */
