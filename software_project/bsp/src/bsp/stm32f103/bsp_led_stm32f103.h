#ifndef _BSP_LED_STM32F103_H_
#define _BSP_LED_STM32F103_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>
#include "stm32f1xx_ll_gpio.h"

/// LED配置
typedef struct Bsp_TLedCfgTag Bsp_TLedCfg;
struct Bsp_TLedCfgTag
{
    GPIO_TypeDef* port;             ///< 端口
    UInt32 pin;                     ///< 引脚
    Bool default_level;             ///< 默认电平
    Bool is_reverse;                ///< 是否反转
};

/// LED项
typedef struct Bsp_TLedItemTag Bsp_TLedItem;
struct Bsp_TLedItemTag
{
    UInt8 id;                       ///< 标识 
    const Bsp_TLedCfg* cfg;         ///< LED配置
    Bool is_on;                     ///< LED使能标志
};

/// LED集合
typedef struct Bsp_TLedUnionTag Bsp_TLedUnion;
struct Bsp_TLedUnionTag
{
    Int32 count;
    Bsp_TLedItem* items;
};

/// 外部定义全局LED集合对象
extern Bsp_TLedUnion g_bsp_led_union;

void Bsp_LedInit_(void);

void Bsp_LedDone_(void);

void Bsp_LedOpen_(void);

void Bsp_LedClose_(void);

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_LED_STM32F103_H_ */
