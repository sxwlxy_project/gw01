#ifndef _BSP_MISC_CTRL_STM32F103_H_
#define _BSP_MISC_CTRL_STM32F103_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>
#include "stm32f1xx_ll_gpio.h"

/// 混合控制信号配置
typedef struct Bsp_TMiscCtrlCfgTag Bsp_TMiscCtrlCfg;
struct Bsp_TMiscCtrlCfgTag
{
    GPIO_TypeDef* port;             ///< 端口
    UInt32 pin;                     ///< 引脚
    UInt32 output_type;             ///< 输出类型
    Bool default_level;             ///< 默认电平
    Bool is_reverse;                ///< 是否反转
};

/// 混合控制信号项
typedef struct Bsp_TMiscCtrlItemTag Bsp_TMiscCtrlItem;
struct Bsp_TMiscCtrlItemTag
{
    UInt8 id;                       ///< 标识 
    const Bsp_TMiscCtrlCfg* cfg;    ///< 配置
    Bool is_on;                     ///< 使能标志
};

/// 混合控制信号集合
typedef struct Bsp_TMiscCtrlUnionTag Bsp_TMiscCtrlUnion;
struct Bsp_TMiscCtrlUnionTag
{
    Int32 count;
    Bsp_TMiscCtrlItem* items;
};

/// 外部定义全局混合控制信号集合对象
extern Bsp_TMiscCtrlUnion g_bsp_misc_ctrl_union;

void Bsp_MiscCtrlInit_(void);

void Bsp_MiscCtrlDone_(void);

void Bsp_MiscCtrlOpen_(void);

void Bsp_MiscCtrlClose_(void);

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_MISC_CTRL_STM32F103_H_ */
