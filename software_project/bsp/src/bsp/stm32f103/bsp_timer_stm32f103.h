#ifndef _BSP_TIMER_STM32F103_H_
#define _BSP_TIMER_STM32F103_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>
#include "stm32f1xx_ll_tim.h"
#include "bsp_timer.h"

#define BSP_TIMER_ITEM_MAX_COUNT    (10)

typedef struct Bsp_TTimerItemTag Bsp_TTimerItem;
struct Bsp_TTimerItemTag
{
    void *user_data;                    ///< 用户数据
    Bsp_TOnTimerEvent on_timer;         ///< 定时事件; 注: 该事件在中断中调用
    UInt32 interval;                    ///< 定时间隔
    UInt32 tick_count;                  ///< 当前计数值
    Bool enable;                        ///< 定时器是否使能
    Bool is_used;                       ///< 是否在使用
};

typedef struct Bsp_TTimerUnionTag Bsp_TTimerUnion;
struct Bsp_TTimerUnionTag
{
    Bsp_TTimerItem items[BSP_TIMER_ITEM_MAX_COUNT];  ///< 定时器列表
    Int32 count;  ///< 当前的定时器个数
    UInt32 tick_count;  ///< 总tick计数
    Bool is_open;  ///< 是否已经打开
};

extern Bsp_TTimerUnion g_bsp_timer_union;

void Bsp_TimerInit_(void);

void Bsp_TimerDone_(void);

void Bsp_TimerOpen_(void);

void Bsp_TimerClose_(void);

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_TIMER_STM32F103_H_ */
