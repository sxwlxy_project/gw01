#include "bsp_common_stm32f103.h"
#include "bsp_switch_stm32f103.h"

void Bsp_SwitchInit_(void)
{
}

void Bsp_SwitchDone_(void)
{
}

void Bsp_SwitchOpen_(void)
{
    Bsp_TSwitchUnion* self = &g_bsp_switch_union;
    Bsp_TSwitchItem* item;
    const Bsp_TSwitchCfg* cfg;

    for (Int32 i = 0; i < self->count; i++)
    {
        item = &self->items[i];
        cfg = item->cfg;

        /// 配置引脚为输入
        LL_GPIO_SetPinMode(cfg->port, cfg->pin, LL_GPIO_MODE_FLOATING);

        /// 配置引脚为上拉
        if (i == Bsp_kSwitchIdPb5 || i == Bsp_kSwitchIdPb6 || i == Bsp_kSwitchIdPb7)
        {
            LL_GPIO_SetPinMode(cfg->port, cfg->pin, LL_GPIO_MODE_INPUT);
            LL_GPIO_SetPinPull(cfg->port, cfg->pin, LL_GPIO_PULL_UP);
        }
    }
}

void Bsp_SwitchClose_(void)
{
    
}

Int32 Bsp_SwitchGetStatus(Int32 id, Bool* status)
{
    Int32 result;
    Bsp_TSwitchUnion* self = &g_bsp_switch_union;
    Int32 index;

    BSP_GET_INDEX(index, id);
    if (index >= 0)
    {
        Bsp_TSwitchItem* item = &self->items[index];

        *status = LL_GPIO_IsInputPinSet(item->cfg->port, item->cfg->pin);

        result = Bsp_kErrorCodeSuccess;
    }
    else
        result = Bsp_kErrorCodeParam;

    return result;
}
