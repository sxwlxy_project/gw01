#ifndef _BSP_COMMON_STM32F103_H_
#define _BSP_COMMON_STM32F103_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>

/// 获取self中item->id = id的item所在的序号，保存在index中
#define BSP_GET_INDEX(index, id)    do{\
                                        index = self->count - 1;\
                                        for(; index >= 0 && self->items[index].id != (id); index--);\
                                    } while (0);

/// 判断handle是否合法
#define BSP_ASSERT_HANDLE(is_ok, handle)    do{\
                                                is_ok = (void*)&self->items[0] == (void*)handle;\
                                                for (Int32 i = 1; \
                                                    &self->items[i] < &self->items[self->count] && !is_ok; \
                                                    i++)\
                                                    is_ok = (void*)&self->items[i] == (void*)handle;\
                                            } while(0);

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_COMMON_STM32F103_H_ */
