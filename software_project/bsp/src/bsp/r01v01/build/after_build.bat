echo off

set CUR_PATH=%~dp0
set TOP_PATH=%CUR_PATH%..\..\..\..

copy %CUR_PATH%..\stm32f103\inc\*.h  %TOP_PATH%\output\bsp\include\   /y
copy %CUR_PATH%..\..\base\inc\*.h  %TOP_PATH%\output\bsp\include\   /y
copy %CUR_PATH%Objects\bsp.lib  %TOP_PATH%\output\bsp\lib\   /y
