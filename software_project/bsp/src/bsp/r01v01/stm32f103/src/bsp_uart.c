enum
{
    Bsp_kUartId0             = 0,                   ///< USART1, 被激光传感器使用
    Bsp_kUartId1             = 1,                   ///< USART2, 被陀螺仪占用
};

#include "bsp_uart_cfg.c"
#include "bsp_uart_stm32f103.c"
#include "drv_uart_stm32f103.c"
