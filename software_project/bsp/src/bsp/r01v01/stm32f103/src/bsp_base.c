#include "bsp_inc_all.h"

static Bsp_TVersion g_bsp_version = 
{
    .ver = 0,
    .build_time = 1711256007,
    .desc = "Bsp Demo Frist stage\r\n"
};

Int32 Bsp_Init(void)
{
    Bsp_CLockInit_();
    Bsp_CLockOpen_();

    Bsp_TimerInit_();
    Bsp_MotorInit_();
    Bsp_UartInit_();
    Bsp_MiscCtrlInit_();
    Bsp_LedInit_();
    Bsp_SwitchInit_();
    
    return Bsp_kErrorCodeSuccess;
}

Int32 Bsp_Done(void)
{
    Bsp_SwitchDone_();
    Bsp_LedDone_();
    Bsp_MiscCtrlDone_();
    Bsp_UartDone_();
    Bsp_MotorDone_();
    Bsp_TimerDone_();

    Bsp_CLockClose_();
    Bsp_CLockDone_();

    return Bsp_kErrorCodeSuccess;
}

Int32 Bsp_Open(void)
{
    Bsp_TimerOpen_();
    Bsp_MotorOpen_();
    Bsp_UartOpen_();
    Bsp_MiscCtrlOpen_();
    Bsp_LedOpen_();
    Bsp_SwitchOpen_();

    return Bsp_kErrorCodeSuccess;
}

Int32 Bsp_Close(void)
{
    Bsp_SwitchClose_();
    Bsp_LedClose_();
    Bsp_MiscCtrlClose_();
    Bsp_UartClose_();
    Bsp_MotorClose_();
    Bsp_TimerClose_();

    return Bsp_kErrorCodeSuccess;
}

Int32 Bsp_GetVersion(Bsp_TVersion* version)
{
    Int32 result;

    if (version == NULL)
        result = Bsp_kErrorCodeParam;
    else
    {
        *version = g_bsp_version;
        result = Bsp_kErrorCodeSuccess;
    }

    return result;
}
