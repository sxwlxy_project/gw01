#ifndef _BSP_INC_ALL_H_
#define _BSP_INC_ALL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>

#include "bsp_inc.h"

#include "bsp_axis_stm32f103.h"
#include "bsp_clock_stm32f103.h"
#include "bsp_led_stm32f103.h"
#include "bsp_misc_ctrl_stm32f103.h"
#include "bsp_motor_stm32f103.h"
#include "bsp_switch_stm32f103.h"
#include "bsp_timer_stm32f103.h"
#include "bsp_uart_stm32f103.h"

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_INC_ALL_H_ */
