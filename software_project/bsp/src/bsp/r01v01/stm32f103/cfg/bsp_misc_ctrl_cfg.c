#include "bsp_inc.h"
#include "bsp_misc_ctrl_stm32f103.h"

static const Bsp_TMiscCtrlCfg g_bsp_misc_ctrl_cfgs_[] = 
{
    {   /// PD0, 推挽输出
        .port = GPIOD,
        .pin = LL_GPIO_PIN_0,
        .output_type = LL_GPIO_OUTPUT_PUSHPULL,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PD1, 推挽输出
        .port = GPIOD,
        .pin = LL_GPIO_PIN_1,
        .output_type = LL_GPIO_OUTPUT_PUSHPULL,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PD2, 推挽输出
        .port = GPIOD,
        .pin = LL_GPIO_PIN_2,
        .output_type = LL_GPIO_OUTPUT_PUSHPULL,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PD3, 推挽输出
        .port = GPIOD,
        .pin = LL_GPIO_PIN_3,
        .output_type = LL_GPIO_OUTPUT_PUSHPULL,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PD4, 推挽输出
        .port = GPIOD,
        .pin = LL_GPIO_PIN_4,
        .output_type = LL_GPIO_OUTPUT_PUSHPULL,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PD5, 推挽输出
        .port = GPIOD,
        .pin = LL_GPIO_PIN_5,
        .output_type = LL_GPIO_OUTPUT_PUSHPULL,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PE2, 推挽输出
        .port = GPIOE,
        .pin = LL_GPIO_PIN_2,
        .output_type = LL_GPIO_OUTPUT_PUSHPULL,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PE4, 推挽输出
        .port = GPIOE,
        .pin = LL_GPIO_PIN_4,
        .output_type = LL_GPIO_OUTPUT_PUSHPULL,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PE6, 推挽输出
        .port = GPIOE,
        .pin = LL_GPIO_PIN_6,
        .output_type = LL_GPIO_OUTPUT_PUSHPULL,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PE10, 开漏输出
        .port = GPIOE,
        .pin = LL_GPIO_PIN_10,
        .output_type = LL_GPIO_OUTPUT_OPENDRAIN,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PE11, 开漏输出
        .port = GPIOE,
        .pin = LL_GPIO_PIN_11,
        .output_type = LL_GPIO_OUTPUT_OPENDRAIN,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PE12, 开漏输出
        .port = GPIOE,
        .pin = LL_GPIO_PIN_12,
        .output_type = LL_GPIO_OUTPUT_OPENDRAIN,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PE13, 开漏输出
        .port = GPIOE,
        .pin = LL_GPIO_PIN_13,
        .output_type = LL_GPIO_OUTPUT_OPENDRAIN,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PE14, 开漏输出
        .port = GPIOE,
        .pin = LL_GPIO_PIN_14,
        .output_type = LL_GPIO_OUTPUT_OPENDRAIN,
        .default_level = False,
        .is_reverse = False,
    },
    {   /// PE15, 开漏输出
        .port = GPIOE,
        .pin = LL_GPIO_PIN_15,
        .output_type = LL_GPIO_OUTPUT_OPENDRAIN,
        .default_level = False,
        .is_reverse = False,
    },
};

static Bsp_TMiscCtrlItem g_bsp_misc_ctrl_items_[] = 
{
    {
        .id = Bsp_kMiscCtrlIdPd0,
        .cfg = &g_bsp_misc_ctrl_cfgs_[0],
    },
    {
        .id = Bsp_kMiscCtrlIdPd1,
        .cfg = &g_bsp_misc_ctrl_cfgs_[1],
    },
    {
        .id = Bsp_kMiscCtrlIdPd2,
        .cfg = &g_bsp_misc_ctrl_cfgs_[2],
    },
    {
        .id = Bsp_kMiscCtrlIdPd3,
        .cfg = &g_bsp_misc_ctrl_cfgs_[3],
    },
    {
        .id = Bsp_kMiscCtrlIdPd4,
        .cfg = &g_bsp_misc_ctrl_cfgs_[4],
    },
    {
        .id = Bsp_kMiscCtrlIdPd5,
        .cfg = &g_bsp_misc_ctrl_cfgs_[5],
    },
    {
        .id = Bsp_kMiscCtrlIdPe2,
        .cfg = &g_bsp_misc_ctrl_cfgs_[6],
    },
    {
        .id = Bsp_kMiscCtrlIdPe4,
        .cfg = &g_bsp_misc_ctrl_cfgs_[7],
    },
    {
        .id = Bsp_kMiscCtrlIdPe6,
        .cfg = &g_bsp_misc_ctrl_cfgs_[8],
    },
    {
        .id = Bsp_kMiscCtrlIdPe10,
        .cfg = &g_bsp_misc_ctrl_cfgs_[9],
    },
    {
        .id = Bsp_kMiscCtrlIdPe11,
        .cfg = &g_bsp_misc_ctrl_cfgs_[10],
    },
    {
        .id = Bsp_kMiscCtrlIdPe12,
        .cfg = &g_bsp_misc_ctrl_cfgs_[11],
    },
    {
        .id = Bsp_kMiscCtrlIdPe13,
        .cfg = &g_bsp_misc_ctrl_cfgs_[12],
    },
    {
        .id = Bsp_kMiscCtrlIdPe14,
        .cfg = &g_bsp_misc_ctrl_cfgs_[13],
    },
    {
        .id = Bsp_kMiscCtrlIdPe15,
        .cfg = &g_bsp_misc_ctrl_cfgs_[14],
    },
};

Bsp_TMiscCtrlUnion g_bsp_misc_ctrl_union = 
{
    .count = sizeof(g_bsp_misc_ctrl_items_) / sizeof(g_bsp_misc_ctrl_items_[0]),
    .items = g_bsp_misc_ctrl_items_,
};
