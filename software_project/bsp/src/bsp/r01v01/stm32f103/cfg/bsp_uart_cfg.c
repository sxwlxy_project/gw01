#include "bsp_inc.h"
#include "bsp_uart_stm32f103.h"

static const Bsp_TUartCfg g_bsp_uart_cfgs_[] =
{
    {
        .uart_base = USART1,
        .dma_base = DMA1,
        .dma_tx_ch = LL_DMA_CHANNEL_4,
        .dma_rx_ch = LL_DMA_CHANNEL_5,
        .is_dma = True,
    },
    {
        .uart_base = USART2,
        .dma_base = DMA1,
        .dma_tx_ch = LL_DMA_CHANNEL_7,
        .dma_rx_ch = LL_DMA_CHANNEL_6,
        .is_dma = True,
    },
    {
        .uart_base = USART3,
        .dma_base = DMA1,
        .dma_tx_ch = LL_DMA_CHANNEL_2,
        .dma_rx_ch = LL_DMA_CHANNEL_3,
        .is_dma = True,
    },
    {
        .uart_base = UART4,
        .dma_base = DMA2,
        .dma_tx_ch = LL_DMA_CHANNEL_5,
        .dma_rx_ch = LL_DMA_CHANNEL_3,
        .is_dma = True,
    },
    {
        .uart_base = UART5,
        .is_dma = False,
    },
};

static Bsp_TUartItem g_bsp_uart_items_[] = 
{
    {
        .id = Bsp_kUartId0,
        .cfg = &g_bsp_uart_cfgs_[0],
    },
    {
        .id = Bsp_kUartId1,
        .cfg = &g_bsp_uart_cfgs_[1],
    },
    {
        .id = Bsp_kUartId2,
        .cfg = &g_bsp_uart_cfgs_[2],
    },
    {
        .id = Bsp_kUartId3,
        .cfg = &g_bsp_uart_cfgs_[3],
    },
    {
        .id = Bsp_kUartId4,
        .cfg = &g_bsp_uart_cfgs_[4],
    },
};

Bsp_TUartUnion g_bsp_uart_union = 
{
    .count = sizeof(g_bsp_uart_items_) / sizeof(g_bsp_uart_items_[0]),
    .items = g_bsp_uart_items_,
};
