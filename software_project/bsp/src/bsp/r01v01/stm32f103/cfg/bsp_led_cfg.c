#include "bsp_inc.h"
#include "bsp_led_stm32f103.h"

static const Bsp_TLedCfg g_bsp_led_cfgs_[] = 
{
    {   /// PC13
        .port = GPIOC,
        .pin = LL_GPIO_PIN_13,
        .default_level = True,
        .is_reverse = True,
    },
};

static Bsp_TLedItem g_bsp_led_items_[] = 
{
    {
        .id = Bsp_kLedId0,
        .cfg = &g_bsp_led_cfgs_[0],
    },
};

Bsp_TLedUnion g_bsp_led_union = 
{
    .count = sizeof(g_bsp_led_items_) / sizeof(g_bsp_led_items_[0]),
    .items = g_bsp_led_items_,
};
