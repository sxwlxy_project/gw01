#include "bsp_laser_stm32f103.h"
#include "bsp_uart.h"
#include "bsp_uart_def.h"

/// 硬件读取数据的回调函数
/// >= 0返回实际读到的字节个数, < 0表示读取失败
static int Bsp_LaserOnReadBytes_(Bsp_TLaserAttr* self, void* buf, int size)
{
    return Bsp_UartRead(self->laser_handle, buf, size);
}

/// 硬件发送数据的回调函数
/// >= 0返回实际发送的字节个数, < 0表示发送失败
static int Bsp_LaserOnWriteBytes_(Bsp_TLaserAttr* self, void* data, int size)
{
    return Bsp_UartWrite(self->laser_handle, data, size);
}

static Int8 g_bsp_laser_recv_buf[256];
static Int8 g_bsp_laser_send_buf[10];
static Int8 g_bsp_laser_read_buf[100];

Bsp_TLaserAttr g_bsp_laser_attr = 
{
    .on_read = Bsp_LaserOnReadBytes_,
    .on_write = Bsp_LaserOnWriteBytes_,
    .recv_buf = g_bsp_laser_recv_buf,
    .send_buf = g_bsp_laser_send_buf,
    .recv_buf_size = sizeof(g_bsp_laser_recv_buf),
    .send_buf_size = sizeof(g_bsp_laser_send_buf),

    .read_buf = g_bsp_laser_read_buf,
    .read_buf_size = sizeof(g_bsp_laser_read_buf),

    .laser_id = 0,
};
