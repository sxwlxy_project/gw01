#include "bsp_inc.h"
#include "bsp_switch_stm32f103.h"

static const Bsp_TSwitchCfg g_bsp_switch_cfgs_[] = 
{
    {   /// PB5
        .port = GPIOB,
        .pin = LL_GPIO_PIN_5,
    },
    {   /// PB6
        .port = GPIOB,
        .pin = LL_GPIO_PIN_6,
    },
    {   /// PB7
        .port = GPIOB,
        .pin = LL_GPIO_PIN_7,
    },
    {   /// PB8
        .port = GPIOB,
        .pin = LL_GPIO_PIN_8,
    },
    {   /// PB9
        .port = GPIOB,
        .pin = LL_GPIO_PIN_9,
    },
};

static Bsp_TSwitchItem g_bsp_switch_items_[] = 
{
    {
        .id = Bsp_kSwitchIdPb5,
        .cfg = &g_bsp_switch_cfgs_[0],
    },
    {
        .id = Bsp_kSwitchIdPb6,
        .cfg = &g_bsp_switch_cfgs_[1],
    },
    {
        .id = Bsp_kSwitchIdPb7,
        .cfg = &g_bsp_switch_cfgs_[2],
    },
    {
        .id = Bsp_kSwitchIdPb8,
        .cfg = &g_bsp_switch_cfgs_[3],
    },
    {
        .id = Bsp_kSwitchIdPb9,
        .cfg = &g_bsp_switch_cfgs_[4],
    },
};

Bsp_TSwitchUnion g_bsp_switch_union = 
{
    .count = sizeof(g_bsp_switch_items_) / sizeof(g_bsp_switch_items_[0]),
    .items = g_bsp_switch_items_,
};
