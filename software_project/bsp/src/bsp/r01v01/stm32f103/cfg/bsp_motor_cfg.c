#include "bsp_inc.h"
#include "bsp_motor_stm32f103.h"

static Bsp_TMotorItem g_bsp_motor_items_[] = 
{
    {
        .id = Bsp_kMotorIdFrontLeft,
    },
    {
        .id = Bsp_kMotorIdFrontRight,
    },
    {
        .id = Bsp_kMotorIdRearLeft,
    },
    {
        .id = Bsp_kMotorIdRearRight,
    },
};

Bsp_TMotorUnion g_bsp_motor_union = 
{
    .count = sizeof(g_bsp_motor_items_) / sizeof(g_bsp_motor_items_[0]),
    .items = g_bsp_motor_items_,
};
