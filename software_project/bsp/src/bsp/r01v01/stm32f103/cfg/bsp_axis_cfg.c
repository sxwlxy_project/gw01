#include "bsp_axis_stm32f103.h"
#include "bsp_uart.h"
#include "bsp_uart_def.h"

/// 硬件读取数据的回调函数
/// >= 0返回实际读到的字节个数, < 0表示读取失败
static int Bsp_AxisOnReadBytes_(Bsp_TAxisAttr* self, void* buf, int size)
{
    return Bsp_UartRead(self->axis_handle, buf, size);
}

/// 硬件发送数据的回调函数
/// >= 0返回实际发送的字节个数, < 0表示发送失败
static int Bsp_AxisOnWriteBytes_(Bsp_TAxisAttr* self, void* data, int size)
{
    return Bsp_UartWrite(self->axis_handle, data, size);
}

static Int8 g_bsp_axis_recv_buf[512];
static Int8 g_bsp_axis_send_buf[100];
static Int8 g_bsp_axis_read_buf[100];

Bsp_TAxisAttr g_bsp_axis_attr = 
{
    .on_read = Bsp_AxisOnReadBytes_,
    .on_write = Bsp_AxisOnWriteBytes_,
    .recv_buf = g_bsp_axis_recv_buf,
    .send_buf = g_bsp_axis_send_buf,
    .recv_buf_size = sizeof(g_bsp_axis_recv_buf),
    .send_buf_size = sizeof(g_bsp_axis_send_buf),

    .read_buf = g_bsp_axis_read_buf,
    .read_buf_size = sizeof(g_bsp_axis_read_buf),

    .axis_id = 1,
};
