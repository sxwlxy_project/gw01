#ifndef _BSP_LASER_DEF_H_
#define _BSP_LASER_DEF_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "BaseTypes.h"

/// 激光传感器数据结构
typedef struct Bsp_TLaserDataTag Bsp_TLaserData;
struct Bsp_TLaserDataTag
{
    UInt16 distance1;       ///< 距离1, 单位mm
    UInt16 distance2;       ///< 距离2, 单位mm
    UInt16 distance3;       ///< 距离3, 单位mm
    UInt16 distance4;       ///< 距离4, 单位mm
};

/// 激光传感器数据类型定义
typedef enum Bsp_TLaserDataTypeTag Bsp_TLaserDataType;
enum Bsp_TLaserDataTypeTag
{
    Bsp_kLaserDataTypeBase              = 0,
    Bsp_kLaserDataTypeCollectBoard      = Bsp_kLaserDataTypeBase,      ///< 采集板数据
    Bsp_kLaserDataTypeMax,
};

/// 激光传感器ID定义
typedef enum Bsp_TLaserIdTag Bsp_TLaserId;
enum Bsp_TLaserIdTag
{
    Bsp_kLaserIdBase                    = 0,
    Bsp_kLaserIdCollectBoard            = Bsp_kLaserIdBase,      ///< 激光传感器采集板
    Bsp_kLaserIdMax,
};

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif /** _BSP_LASER_DEF_H_ */
