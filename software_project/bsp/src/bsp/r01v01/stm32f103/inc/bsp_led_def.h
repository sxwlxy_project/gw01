#ifndef _BSP_LED_DEF_H_
#define _BSP_LED_DEF_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>

/// LED标识
typedef enum Bsp_TLedIdTag Bsp_TLedId;
enum Bsp_TLedIdTag
{
    Bsp_kLedIdBase          = 0,
    Bsp_kLedId0             = Bsp_kLedIdBase,
    Bsp_kLedIdMax,
};

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_LED_DEF_H_ */
