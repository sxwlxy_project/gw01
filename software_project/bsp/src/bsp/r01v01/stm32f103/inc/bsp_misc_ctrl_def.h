#ifndef _BSP_MISC_CTRL_DEF_H_
#define _BSP_MISC_CTRL_DEF_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>

/// 混合控制标识
typedef enum Bsp_TMiscCtrlIdTag Bsp_TMiscCtrlId;
enum Bsp_TMiscCtrlIdTag
{
    Bsp_kMiscCtrlIdBase             = 0,
    Bsp_kMiscCtrlIdPd0              = Bsp_kMiscCtrlIdBase,      ///< PD0
    Bsp_kMiscCtrlIdPd1              = 1,                        ///< PD1
    Bsp_kMiscCtrlIdPd2              = 2,                        ///< PD2
    Bsp_kMiscCtrlIdPd3              = 3,                        ///< PD3
    Bsp_kMiscCtrlIdPd4              = 4,                        ///< PD4
    Bsp_kMiscCtrlIdPd5              = 5,                        ///< PD5
    Bsp_kMiscCtrlIdPe2              = 6,                        ///< PE2
    Bsp_kMiscCtrlIdPe4              = 7,                        ///< PE4
    Bsp_kMiscCtrlIdPe6              = 8,                        ///< PE6
    Bsp_kMiscCtrlIdPe10             = 9,                        ///< PE10
    Bsp_kMiscCtrlIdPe11             = 10,                       ///< PE11
    Bsp_kMiscCtrlIdPe12             = 11,                       ///< PE12
    Bsp_kMiscCtrlIdPe13             = 12,                       ///< PE13
    Bsp_kMiscCtrlIdPe14             = 13,                       ///< PE14
    Bsp_kMiscCtrlIdPe15             = 14,                       ///< PE15
    Bsp_kMiscCtrlIdMax,
};

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_MISC_CTRL_DEF_H_ */
