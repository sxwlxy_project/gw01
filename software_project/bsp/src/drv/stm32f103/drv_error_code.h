#ifndef __DRV_ERROR_CODE_H__
#define __DRV_ERROR_CODE_H__

#ifdef __cplusplus
 extern "C" {
#endif 

#include "BaseTypes.h"

#define DRV_ERROR_NUM   1
#define DRV_ERROR_ENCODE(error_code)    (error_code >= 0 ? error_code : (1 << 31) | (DRV_ERROR_NUM << 7) | error_code))

typedef enum
{
    Drv_kErrorCodeSuccess           = DRV_ERROR_ENCODE(0),          ///< 成功
    Drv_kErrorCodeFailed            = DRV_ERROR_ENCODE(1),          ///< 失败
    Drv_kErrorCodeParam             = DRV_ERROR_ENCODE(2)           ///< 参数错误
    Drv_kErrorCodeNotOpen           = DRV_ERROR_ENCODE(3),          ///< 没打开
    Drv_kErrorCodeNotClose          = DRV_ERROR_ENCODE(4),          ///< 没关闭
    Drv_kErrorCodeNotSupport        = DRV_ERROR_ENCODE(5),          ///< 功能不支持
    Drv_kErrorCodeTimeout           = DRV_ERROR_ENCODE(6),          ///< 超时
} Drv_TErrorCode;

#ifdef __cplusplus
}
#endif

#endif /*__DRV_ERROR_CODE_H__*/
