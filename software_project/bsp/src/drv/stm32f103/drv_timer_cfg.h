#ifndef __DRV_TIMER_CFG_H__
#define __DRV_TIMER_CFG_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include "BaseTypes.h"
#include "stm32f0xx_ll_tim.h"

/// timer 外设枚举
typedef enum
{
    kDrv_Timer0               = (UInt32)TIM1,     ///< TIM1 暂时不能使用TIM1
    kDrv_Timer1               = (UInt32)TIM3,     ///< TIM3
    kDrv_Timer2               = (UInt32)TIM6,     ///< TIM6
    kDrv_Timer3               = (UInt32)TIM14,     ///< TIM14
    kDrv_Timer4               = (UInt32)TIM15,     ///< TIM15
    kDrv_Timer5               = (UInt32)TIM16,     ///< TIM16
    kDrv_Timer6               = (UInt32)TIM17,     ///< TIM17
} TDrv_TimerX;

#define DRV_TIMER_COUNT     (7)     ///< 可用的定时器个数

/// timer 配置
typedef struct TDrv_Timer_CfgTag    TDrv_Timer_Cfg;
struct TDrv_Timer_CfgTag
{
    TDrv_TimerX timx;           ///< 定时器x
};

#ifdef __cplusplus
}
#endif

#endif /**__DRV_TIMER_CFG_H__*/
