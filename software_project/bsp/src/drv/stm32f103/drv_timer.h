#ifndef __DRV_TIMER_H__
#define __DRV_TIMER_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include "BaseTypes.h"
#include "drv_error_code.h"
#include "drv_timer_cfg.h"

/// 定时器更新事件
typedef void (*TDrv_Timer_OnUpdateEventCallback)(void* user_param);
typedef struct
{
    TDrv_Timer_OnUpdateEventCallback event;
    void* user_param;
}TDrv_Timer_OnUpdateEvent;

/// timer 外设对象
typedef struct TDrv_TimerTag  TDrv_Timer;
struct TDrv_TimerTag
{
    const void*                 cfg;        ///< 配置信息
    TDrv_Timer_OnUpdateEvent    event;      ///< 定时器更新事件
    UInt16                      update_ms;  ///< 定时器多少ms更新
    Bool                        is_open;    ///< 是否打开
};

/**
 * @brief 初始化timer对象
 * @param timer : timer对象，参见[@TDrv_Timer]
 *        cfg : timer配置
 * @retval 0   : 成功
 *         < 0 : 失败，参见[@TDrv_Error_Code]
*/
Int32 TDrv_Timer_Init(TDrv_Timer* timer, const void* cfg);

/**
 * @brief 去初始化Timer对象
 * @param Timer : Timer对象，参见[@TDrv_Timer]
 * @retval 0   : 成功
 *         < 0 : 失败，参见[@TDrv_Error_Code]
*/
Int32 TDrv_Timer_DeInit(TDrv_Timer* timer);

/**
 * @brief 设置定时器多少ms更新一次
 * @note 在 TDrv_Timer_Open 之前调用
 * @param Timer : Timer对象，参见[@TDrv_Timer]
 *        ms : 多少ms中断一次, 最大值为65535ms, 0表示不更新
*/
void TDrv_Timer_SetUpdateMs(TDrv_Timer* timer, UInt16 ms);

/**
 * @brief 打开Timer
 * @param Timer : Timer对象，参见[@TDrv_Timer]
 * @retval 0   : 成功
 *         < 0 : 失败，参见[@TDrv_Error_Code]
*/
Int32 TDrv_Timer_Open(TDrv_Timer* timer);

/**
 * @brief 关闭Timer
 * @param Timer : Timer对象，参见[@TDrv_Timer]
 * @retval 0   : 成功
 *         < 0 : 失败，参见[@TDrv_Error_Code]
*/
Int32 TDrv_Timer_Close(TDrv_Timer* timer);

/**
 * @brief 获取Timer是打开状态还是关闭状态
 * @param Timer : Timer对象，参见[@TDrv_Timer]
 * @retval 1 : 打开
 *         0 : 关闭
*/
Int32 TDrv_Timer_IsOpen(TDrv_Timer* timer);

/**
 * @brief 设置定时器更新事件
 * @param Timer : Timer对象，参见[@TDrv_Timer]
 *        event_obj : 更新事件，参见[@TDrv_Timer_OnUpdateEvent]
 * @retval 0   : 成功
 *         < 0 : 失败，参见[@TDrv_Error_Code]
*/
Int32 TDrv_Timer_SetUpdateEvent(TDrv_Timer* timer, TDrv_Timer_OnUpdateEvent event_obj);

#ifdef __cplusplus
}
#endif

#endif /**__DRV_TIMER_H__*/
