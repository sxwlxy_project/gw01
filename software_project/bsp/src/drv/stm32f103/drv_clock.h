#ifndef __DRV_CLOCK_H__
#define __DRV_CLOCK_H__

#ifdef __cplusplus
 extern "C" {
#endif 

#include "BaseTypes.h"

typedef enum Drv_TClockSourceTag Drv_TClockSource;
enum Drv_TClockSourceTag
{
    Drv_kClockSourceSysclk      = 0,    ///< 系统时钟
    Drv_kClockSourceHclk        = 1,    ///< HCLK时钟
    Drv_kClockSourcePclk1       = 2,    ///< PCLK1时钟
    Drv_kClockSourcePclk2       = 3,    ///< PCLK2时钟
};

/**
 * @brief 初始化时钟
*/
void Drv_ClockInit(void);

/**
 * @brief 去初始化时钟
*/
void Drv_ClockDone(void);

/**
 * @brief 获取时钟源频率
*/
Int32 Drv_ClockGetFrequency(Drv_TClockSource source);

#ifdef __cplusplus
}
#endif

#endif /*__DRV_CLOCK_H__*/
