// #ifndef __DRV_ADC_H__
// #define __DRV_ADC_H__

// #ifdef __cplusplus
//  extern "C" {
// #endif

// #include "BaseTypes.h"
// #include "drv_error_code.h"
// #include "drv_adc_cfg.h"

// /// ADC外设对象
// typedef struct TDrv_AdcTag  TDrv_Adc;
// struct TDrv_AdcTag
// {
//     const void*     cfg;        ///< 配置信息
//     Bool            is_open;    ///< 是否打开
// };

// /**
//  * @brief 初始化ADC对象
//  * @param adc : ADC对象，参见[@TDrv_Adc]
//  *        cfg : ADC配置
//  * @retval 0   : 成功
//  *         < 0 : 失败，参见[@TDrv_Error_Code]
// */
// Int32 TDrv_Adc_Init(TDrv_Adc* adc, const void* cfg);

// /**
//  * @brief 去初始化ADC对象
//  * @param adc : ADC对象，参见[@TDrv_Adc]
//  * @retval 0   : 成功
//  *         < 0 : 失败，参见[@TDrv_Error_Code]
// */
// Int32 TDrv_Adc_DeInit(TDrv_Adc* adc);

// /**
//  * @brief 打开ADC
//  * @param adc : ADC对象，参见[@TDrv_Adc]
//  * @retval 0   : 成功
//  *         < 0 : 失败，参见[@TDrv_Error_Code]
// */
// Int32 TDrv_Adc_Open(TDrv_Adc* adc);

// /**
//  * @brief 关闭ADC
//  * @param adc : ADC对象，参见[@TDrv_Adc]
//  * @retval 0   : 成功
//  *         < 0 : 失败，参见[@TDrv_Error_Code]
// */
// Int32 TDrv_Adc_Close(TDrv_Adc* adc);

// /**
//  * @brief 读取ADC通道转换值
//  * @note 使用Adc对象的默认转换分辨率
//  * @param adc : ADC对象，参见[@TDrv_Adc]
//  *        channel : 通道
//  * @retval >= 0 : 转化结果
//  *         <  0 : 失败，参见[@TDrv_Error_Code]
// */
// Int32 TDrv_Adc_ReadChannel(TDrv_Adc* adc, UInt32 channel);

// /**
//  * @brief 读取ADC通道转换值(额外的)
//  * @note 使用指定的分辨率转换
//  * @param adc : ADC对象，参见[@TDrv_Adc]
//  *        channel : 通道
//  *        bits : 指定转换分辨率
//  * @retval >= 0 : 转化结果
//  *         <  0 : 失败，参见[@TDrv_Error_Code]
// */
// Int32 TDrv_Adc_ReadChannelEx(TDrv_Adc* adc, UInt32 channel, UInt32 bits);

// #ifdef __cplusplus
// }
// #endif

// #endif /**__DRV_ADC_H__*/
