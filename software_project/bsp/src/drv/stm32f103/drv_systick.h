#ifndef __DRV_SYSTICK_H__
#define __DRV_SYSTICK_H__

#ifdef __cplusplus
 extern "C" {
#endif 

#include "BaseTypes.h"

/// 时基产生事件
typedef void (*TDrv_Systick_OnTickEventCallBack)(void* param);
typedef struct
{
    TDrv_Systick_OnTickEventCallBack event;
    void* param;
}TDrv_Systick_OnTickEvent;

/**
 * @brief 初始化Systick，并启动，配置成1ms时基
 * @param 此函数调用之前需要先调用 Drv_Clock_Init
*/
void Drv_Systick_Init(void);

/**
 * @brief 去初始化Systick, 时基并不会停
*/
void Drv_Systick_DeInit(void);

/**
 * @brief 获取Systick自身的tick值
*/
UInt32 Drv_Systick_GetTick(void);

/**
 * @brief 设置时基产生事件
 * @param event : 时基产生事件
*/
void Drv_Systic_SetTickEvent(TDrv_Systick_OnTickEvent event);

#ifdef __cplusplus
}
#endif

#endif /*__DRV_SYSTICK_H__*/
