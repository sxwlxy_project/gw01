// #include "drv_adc.h"
// #include "drv_adc_cfg.h"
// #include "stm32f0xx_ll_adc.h"
// #include "stm32f0xx_ll_bus.h"
// #include "stm32f0xx_ll_gpio.h"

// enum 
// {
//     kDrv_Adc_Timeout    =   10000,      ///< adc转换的超时时间
// };

// /** --------------- adc的内部配置 --------------- */

// typedef void (*_TDrv_Adc_ClockEnable)(uint32_t);
// typedef void (*_TDrv_Adc_ClockDisable)(uint32_t);

// /// 定时器bus定义
// const static UInt32 g_drv_adc_bus[] = 
// {
//     LL_APB1_GRP2_PERIPH_ADC1
// };

// /// 总线使能函数定义
// const static _TDrv_Adc_ClockEnable g_drv_adc_clock_enable[] = 
// {
//     LL_APB1_GRP2_EnableClock
// };

// /// 总线失能函数定义
// const static _TDrv_Adc_ClockDisable g_drv_adc_clock_disable[] = 
// {
//     LL_APB1_GRP2_DisableClock
// };

// typedef struct
// {
//     /// 用序号可以节省一些空间
//     UInt32 adcx;                    ///< adc定义
//     UInt8 bus_index;                ///< 在哪条总线序号
//     UInt8 clock_enable_index;       ///< 总线使能函数序号
//     UInt8 clock_disable_index;      ///< 总线失能函数序号
// }TDrv_Adc_Diff;

// /// 每个adc的内部不同配置
// const static TDrv_Adc_Diff g_drv_adc_diff[DRV_ADC_COUNT] = 
// {
//     {
//         .adcx                   = kDrv_Adc0,
//         .bus_index              = 0,
//         .clock_enable_index     = 0,
//         .clock_disable_index    = 0,
//     }
// };

// /** ------------- END adc的内部配置 ------------- */

// /// 获得adcx宏定义的序号
// /// -1 表示不存在adcx的序号
// static Int8 _Drv_Adc_Index(UInt32 adcx)
// {
//     Int8 i = sizeof(g_drv_adc_diff) / sizeof(g_drv_adc_diff[0]);
//     const TDrv_Adc_Diff* diff = g_drv_adc_diff;

//     for (i = i - 1; i >= 0; i--)
//         if (diff[i].adcx == adcx)
//             break;

//     return i;
// }

// Int32 TDrv_Adc_Init(TDrv_Adc *adc, const void *cfg)
// {
//     Int32 result;
//     if (adc == NULL || cfg == NULL)
//         result = kDrv_Error_Param;
//     else
//     {
//         adc->is_open = False;
//         adc->cfg = cfg;

//         TDrv_Adc_Cfg* adc_cfg = (TDrv_Adc_Cfg*)cfg;
//         Int8 index = _Drv_Adc_Index(adc_cfg->adcx);

//         LL_ADC_InitTypeDef ADC_InitStruct = {0};
//         LL_ADC_REG_InitTypeDef ADC_REG_InitStruct = {0};
//         const TDrv_Adc_Diff* diff = &g_drv_adc_diff[index];

//         /// ADC相关外设时钟使能
//         g_drv_adc_clock_enable[diff->clock_enable_index](g_drv_adc_bus[diff->bus_index]);

//         /// 配置ADC的全局特性：时钟、分辨率、数据对齐和转换次数
//         ADC_InitStruct.Clock = LL_ADC_CLOCK_ASYNC;              ///< 异步模式，使用14MHz的独立adc时钟
//         ADC_InitStruct.DataAlignment = LL_ADC_DATA_ALIGN_RIGHT; ///< 右对齐模式
//         ADC_InitStruct.LowPowerMode = LL_ADC_LP_MODE_NONE;      ///< 低功耗模式关闭
//         ADC_InitStruct.Resolution = adc_cfg->bits;              ///< 分辨率
//         LL_ADC_Init((ADC_TypeDef *)adc_cfg->adcx, &ADC_InitStruct);            ///< ADC初始化

//         ADC_REG_InitStruct.TriggerSource = LL_ADC_REG_TRIG_SOFTWARE;            ///< 软件触发
//         ADC_REG_InitStruct.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_DISABLE;   ///< 
//         ADC_REG_InitStruct.ContinuousMode = LL_ADC_REG_CONV_SINGLE;             ///< 单次转换
//         ADC_REG_InitStruct.DMATransfer = LL_ADC_REG_DMA_TRANSFER_LIMITED;       ///< DMA
//         ADC_REG_InitStruct.Overrun = LL_ADC_REG_OVR_DATA_PRESERVED;             ///< 
//         LL_ADC_REG_Init((ADC_TypeDef *)adc_cfg->adcx, &ADC_REG_InitStruct);  ///<初始化ADC组规则

//         LL_ADC_REG_SetSequencerScanDirection((ADC_TypeDef *)adc_cfg->adcx, LL_ADC_REG_SEQ_SCAN_DIR_FORWARD);   ///< 设置ADC组常规序列器扫描方向
//         LL_ADC_SetSamplingTimeCommonChannels((ADC_TypeDef *)adc_cfg->adcx, LL_ADC_SAMPLINGTIME_1CYCLE_5);      ///< 设置一组信道共有的采样时间

//         LL_ADC_DisableIT_EOC((ADC_TypeDef *)adc_cfg->adcx);     ///< 禁用中断
//         LL_ADC_StartCalibration((ADC_TypeDef *)adc_cfg->adcx);  ///< 启动ADC校准
//         while (LL_ADC_IsCalibrationOnGoing((ADC_TypeDef *)adc_cfg->adcx)) ;
//         result = kDrv_Error_Success;
//     }

//     return result;
// }

// Int32 TDrv_Adc_DeInit(TDrv_Adc *adc)
// {
//     Int32 result;

//     if (adc->is_open)
//         result = kDrv_Error_NotClose;
//     else
//     {
//         TDrv_Adc_Cfg* adc_cfg = (TDrv_Adc_Cfg*)adc->cfg;

//         if (adc_cfg == NULL)
//             result = kDrv_Error_CfgError;
//         else
//         {
//             Int8 index = _Drv_Adc_Index(adc_cfg->adcx);
//             const TDrv_Adc_Diff* diff = &g_drv_adc_diff[index];

//             LL_ADC_DeInit((ADC_TypeDef *)adc_cfg->adcx);

//             /// 关闭外设时钟
//             g_drv_adc_clock_disable[diff->clock_disable_index](g_drv_adc_bus[diff->bus_index]);

//             adc->cfg = NULL;
//             adc->is_open = False;

//             result = kDrv_Error_Success;
//         }
//     }

//     return result;
// }

// Int32 TDrv_Adc_Open(TDrv_Adc *adc)
// {
//     Int32 result;

//     if (adc->is_open)
//         result = kDrv_Error_HasOpened;
//     else
//     {
//         TDrv_Adc_Cfg* cfg = (TDrv_Adc_Cfg*)adc->cfg;

//         LL_ADC_Enable((ADC_TypeDef *)cfg->adcx);

//         adc->is_open = True;
//         result = kDrv_Error_Success;
//     }

//     return result;
// }

// Int32 TDrv_Adc_Close(TDrv_Adc *adc)
// {
//     Int32 result;

//     if (!adc->is_open)
//         result = kDrv_Error_HasClosed;
//     else
//     {
//         adc->is_open = False;
        
//         TDrv_Adc_Cfg* cfg = (TDrv_Adc_Cfg*)adc->cfg;

//         LL_ADC_Disable((ADC_TypeDef *)cfg->adcx);

//         result = kDrv_Error_Success;
//     }

//     return result;
// }

// static UInt32 adc_channel[] = 
// {
//     LL_ADC_CHANNEL_0,
//     LL_ADC_CHANNEL_1,
//     LL_ADC_CHANNEL_2,
//     LL_ADC_CHANNEL_3,
//     LL_ADC_CHANNEL_4,
//     LL_ADC_CHANNEL_5,
//     LL_ADC_CHANNEL_6,
//     LL_ADC_CHANNEL_7,
//     LL_ADC_CHANNEL_8,
//     LL_ADC_CHANNEL_9,
//     LL_ADC_CHANNEL_10,
//     LL_ADC_CHANNEL_11,
//     LL_ADC_CHANNEL_12,
//     LL_ADC_CHANNEL_13,
//     LL_ADC_CHANNEL_14,
//     LL_ADC_CHANNEL_15,
//     LL_ADC_CHANNEL_16,
//     LL_ADC_CHANNEL_17,
//     LL_ADC_CHANNEL_VREFINT,
//     LL_ADC_CHANNEL_TEMPSENSOR,
// };

// Int32 TDrv_Adc_ReadChannel(TDrv_Adc *adc, UInt32 channel)
// {
//     Int32 result;
//     TDrv_Adc_Cfg* cfg = (TDrv_Adc_Cfg*)adc->cfg;
    
//     LL_ADC_REG_SetSequencerChannels((ADC_TypeDef *)cfg->adcx, channel);    ///< 设置通道
//     LL_ADC_REG_StartConversion((ADC_TypeDef *)cfg->adcx);  ///< 启动转换

//     Int32 time = kDrv_Adc_Timeout;
//     while (LL_ADC_IsActiveFlag_EOC((ADC_TypeDef *)cfg->adcx) == 0 && time > 0)
//         time--;

//     if (time > 0)
//     {
//         switch (cfg->bits)
//         {
//             case LL_ADC_RESOLUTION_12B:
//                 result = LL_ADC_REG_ReadConversionData12((ADC_TypeDef *)cfg->adcx);
//                 break;
//             case LL_ADC_RESOLUTION_10B:
//                 result = LL_ADC_REG_ReadConversionData10((ADC_TypeDef *)cfg->adcx);
//                 break;
//             case LL_ADC_RESOLUTION_8B:
//                 result = LL_ADC_REG_ReadConversionData8((ADC_TypeDef *)cfg->adcx);
//                 break;
//             case LL_ADC_RESOLUTION_6B:
//                 result = LL_ADC_REG_ReadConversionData6((ADC_TypeDef *)cfg->adcx);
//                 break;
//             default:
//                 result = LL_ADC_REG_ReadConversionData12((ADC_TypeDef *)cfg->adcx);
//                 break;
//         }
//     }
//     else
//         result = kDrv_Error_Timeout;
    

//     return result;
// }

// Int32 TDrv_Adc_ReadChannelEx(TDrv_Adc *adc, UInt32 channel, UInt32 bits)
// {
//     Int32 result;
//     TDrv_Adc_Cfg* cfg = (TDrv_Adc_Cfg*)adc->cfg;
    
//     LL_ADC_REG_SetSequencerChannels((ADC_TypeDef *)cfg->adcx, channel);    ///< 设置通道
//     LL_ADC_REG_StartConversion((ADC_TypeDef *)cfg->adcx);  ///< 启动转换

//     Int32 time = kDrv_Adc_Timeout;
//     while (LL_ADC_IsActiveFlag_EOC((ADC_TypeDef *)cfg->adcx) == 0 && time > 0)
//         time--;

//     if (time > 0)
//     {
//         switch (bits)
//         {
//             case LL_ADC_RESOLUTION_12B:
//                 result = LL_ADC_REG_ReadConversionData12((ADC_TypeDef *)cfg->adcx);
//                 break;
//             case LL_ADC_RESOLUTION_10B:
//                 result = LL_ADC_REG_ReadConversionData10((ADC_TypeDef *)cfg->adcx);
//                 break;
//             case LL_ADC_RESOLUTION_8B:
//                 result = LL_ADC_REG_ReadConversionData8((ADC_TypeDef *)cfg->adcx);
//                 break;
//             case LL_ADC_RESOLUTION_6B:
//                 result = LL_ADC_REG_ReadConversionData6((ADC_TypeDef *)cfg->adcx);
//                 break;
//             default:
//                 result = LL_ADC_REG_ReadConversionData12((ADC_TypeDef *)cfg->adcx);
//                 break;
//         }
//     }
//     else
//         result = kDrv_Error_Timeout;

//     return result;
// }
