#ifndef __DRV_UART_CFG_H__
#define __DRV_UART_CFG_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include "BaseTypes.h"
#include "stm32f0xx_ll_usart.h"

/// UART模块枚举
typedef enum
{
    kDrv_Uart0               = (UInt32)USART1,     ///< UART0
    kDrv_Uart1               = (UInt32)USART2,     ///< UART1
} TDrv_UartX;

/// ADC通道
typedef enum
{
    kDrv_Adc_Channel0       = LL_ADC_CHANNEL_0,             ///< 通道0
    kDrv_Adc_Channel1       = LL_ADC_CHANNEL_1,             ///< 通道1
    kDrv_Adc_Channel2       = LL_ADC_CHANNEL_2,             ///< 通道2
    kDrv_Adc_Channel3       = LL_ADC_CHANNEL_3,             ///< 通道3
    kDrv_Adc_Channel4       = LL_ADC_CHANNEL_4,             ///< 通道4
    kDrv_Adc_Channel5       = LL_ADC_CHANNEL_5,             ///< 通道5
    kDrv_Adc_Channel6       = LL_ADC_CHANNEL_6,             ///< 通道6
    kDrv_Adc_Channel7       = LL_ADC_CHANNEL_7,             ///< 通道7
    kDrv_Adc_Channel8       = LL_ADC_CHANNEL_8,             ///< 通道8
    kDrv_Adc_Channel9       = LL_ADC_CHANNEL_9,             ///< 通道9
    // kDrv_Adc_Channel10  = LL_ADC_CHANNEL_10,    ///< 通道10
    // kDrv_Adc_Channel11  = LL_ADC_CHANNEL_11,    ///< 通道11
    // kDrv_Adc_Channel12  = LL_ADC_CHANNEL_12,    ///< 通道12
    // kDrv_Adc_Channel13  = LL_ADC_CHANNEL_13,    ///< 通道13
    // kDrv_Adc_Channel14  = LL_ADC_CHANNEL_14,    ///< 通道14
    // kDrv_Adc_Channel15  = LL_ADC_CHANNEL_15,    ///< 通道15
    // kDrv_Adc_Channel16  = LL_ADC_CHANNEL_16,    ///< 通道16
    kDrv_Adc_ChannelVref    = (Int32)LL_ADC_CHANNEL_VREFINT,       ///< 内部参考电压
    kDrv_Adc_ChannelTemp    = (Int32)LL_ADC_CHANNEL_TEMPSENSOR,    ///< 温度传感器电压
} TDrv_Adc_Channelx;

/// ADC默认转化分辨率
typedef enum
{
    kDrv_Adc_Bits6       = 6,        ///< 6位转换结果
    kDrv_Adc_Bits8       = 8,        ///< 8位转换结果
    kDrv_Adc_Bits10      = 10,       ///< 10位转换结果
    kDrv_Adc_Bits12      = 12,       ///< 12位转换结果
} TDrv_Adc_Bitsx;

/// ADC配置结构体
typedef struct TDrv_Adc_CfgTag  TDrv_Adc_Cfg;
struct TDrv_Adc_CfgTag
{
    TDrv_AdcX   adcx;     /// ADC
    UInt8       bits;     /// 此ADC默认转化分辨率
};

/// stm32f030c8t6只有1个adc
extern const TDrv_Adc_Cfg g_brd_analog_drv_cfg[1];

#ifdef __cplusplus
}
#endif

#endif /**__DRV_UART_CFG_H__*/
