#ifndef _BSP_UART_DEF_H_
#define _BSP_UART_DEF_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>

/// 串口标识
typedef enum Bsp_TUartIdTag Bsp_TUartId;
enum Bsp_TUartIdTag
{
    Bsp_kUartIdBase          = 0,
    // Bsp_kUartId0             = Bsp_kUartIdBase,     ///< USART1
    // Bsp_kUartId1             = 1,                   ///< USART2
    Bsp_kUartId2             = 2,                   ///< USART3
    Bsp_kUartId3             = 3,                   ///< UART4
    Bsp_kUartId4             = 4,                   ///< UART5
    Bsp_kUartIdMax,
};

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_UART_DEF_H_ */
