/**
 * @file bsp_error.h
 * @brief 板级支持包错误相关接口
 * @author 彭泽稳
 * @version v1.0
 * @par 修改历史
 * 版本     修改日期        修改人      修改内容
 * v1.0     2024-04-17      彭泽稳      第一版
 * @copyright Copyright (c) 2024, 彭泽稳
*/
#ifndef _BSP_ERROR_H_
#define _BSP_ERROR_H_

#ifdef __cplusplus
extern "C" {
#endif

#define BSP_ERROR_NUM   0
#define BSP_ERROR_ENCODE(error_code)    (error_code >= 0 ? error_code : ((1 << 31) | (BSP_ERROR_NUM << 7) | error_code))

typedef enum Bsp_TErrorCodeTag Bsp_TErrorCode;
enum Bsp_TErrorCodeTag
{
    Bsp_kErrorCodeSuccess           = BSP_ERROR_ENCODE(0),          ///< 成功
    Bsp_kErrorCodeFailed            = BSP_ERROR_ENCODE(1),          ///< 失败
    Bsp_kErrorCodeParam             = BSP_ERROR_ENCODE(2),          ///< 参数错误
    Bsp_kErrorCodeOpened            = BSP_ERROR_ENCODE(3),          ///< 已打开
    Bsp_kErrorCodeClosed            = BSP_ERROR_ENCODE(4),          ///< 已关闭
    Bsp_kErrorCodeNotSupport        = BSP_ERROR_ENCODE(5),          ///< 不支持
    Bsp_kErrorCodeProcess           = BSP_ERROR_ENCODE(6),          ///< 流程错误
};

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_ERROR_H_ */
