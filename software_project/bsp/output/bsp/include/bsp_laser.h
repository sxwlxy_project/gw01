/**
 * @file bsp_laser.h
 * @brief 板级支持包-激光传感器接口
 * @author 彭泽稳
 * @version v1.0
 * @par 修改历史
 * 版本     修改日期        修改人      修改内容
 * v1.0     2024-04-18      彭泽稳      第一版
 * @par 实例
 * Bsp_Init();
 * Bsp_Open();
 * 
 * Bsp_THandle handle = Bsp_LaserCreate(0);
 * 
 * /// 设置激光传感器数据缓存, 并打开
 * Bsp_TLaserData g_data[10];
 * Bsp_LaserSetDataDepth(handle, Bsp_kLaserDataTypeCollectBoard, g_data, sizeof(g_data) / sizeof(g_data[0]));
 * Bsp_LaserOpen(handle);
 * 
 * // 线程中 
 * Bsp_LaserRunOnce(handle);
 * 
 * /// 获取激光传感器数据
 * Bsp_TLaserData read_data;
 * Int32 result = Bsp_LaserGetData(handle, Bsp_kLaserDataTypeCollectBoard, read_data, 1);
 * if (result > 0)
 * {
 *     /// 获取数据成功
 * }
 * 
 * Bsp_LaserClose(handle);
 * Bsp_LaserFree(handle);
 * 
 * Bsp_Close();
 * Bsp_Done();
 * 
 * @copyright Copyright (c) 2024, 彭泽稳
*/
#ifndef _BSP_LASER_H_
#define _BSP_LASER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "BaseTypes.h"
#include "bsp_base.h"
#include "bsp_laser_def.h"

/**
 * 创建激光传感器对象.
 * @param id: [in], 激光传感器标识, 参见Bsp_TLaserId(@ bsp_laser_def.h)
 * @return 如果创建失败返回NULL
 */
Bsp_THandle Bsp_LaserCreate(Int32 id);

/**
 * 销毁激光传感器对象.
 * @param handle: [in], 句柄
 * @return 
 */
void Bsp_LaserFree(Bsp_THandle handle);

/**
 * 打开激光传感器对象.
 * @note 用户必须在调用此函数之前调用 Bsp_LaserSetDataDepth 函数设置激光传感器数据的深度
 * @param handle: [in], 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
 */
Int32 Bsp_LaserOpen(Bsp_THandle handle);

/**
 * 关闭激光传感器对象.
 * @param handle: [in], 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
 */
Int32 Bsp_LaserClose(Bsp_THandle handle);

/**
 * 设置激光传感器数据深度.
 * @note 用户必须保证在打开激光传感器对象之前调用该函数, 否则无效
 * @param handle: [in], 句柄
 * @param data_type: [in], 激光传感器数据的类型, 参见Bsp_TLaserDataType(@ bsp_laser_def.h)
 * @param data: [in], 提供给激光传感器对象的缓存, 用于内部缓存对应类型的数据
 * @param depth: [in], 数据的深度, 内部最大存储depth个的数据, 用户也最多取出depth个的数据
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
 */
Int32 Bsp_LaserSetDataDepth(Bsp_THandle handle, Int8 data_type, void* data, Int32 depth);

/**
 * 获取激光传感器数据.
 * @param handle: [in], 句柄
 * @param data_type: [in], 激光传感器数据的类型, 参见Bsp_TLaserDataType(@ bsp_laser_def.h)
 * @param data: [out], 用于接收获取到的激光传感器数据
 * @param depth: [in], 尝试获取激光传感器数据的个数
 * @return >=0 表示实际获取到的个数, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
 */
Int32 Bsp_LaserGetData(Bsp_THandle handle, Int8 data_type, void* data, Int32 count);

/**
 * 激光传感器模块功能调度一次
 * @note 用户需要在线程中周期调度此函数, 保证激光传感器模块能够正常工作
 * @param handle: [in], 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
*/
Int32 Bsp_LaserRunOnce(Bsp_THandle handle);

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif /** _BSP_LASER_H_ */
