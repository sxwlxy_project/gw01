/**
 * @file bsp_switch.h
 * @brief 板级支持包-开关量接口
 * @author 彭泽稳
 * @version v1.0
 * @par 修改历史
 * 版本     修改日期        修改人      修改内容
 * v1.0     2024-04-18      彭泽稳      第一版
 * @par 实例
 * Bsp_Init();
 * Bsp_Open();
 * 
 * Bool status;
 * Bsp_SwitchGetStatus(0, &status);    ///< 获取开关量0状态
 * 
 * Bsp_Close();
 * Bsp_Done();
 * 
 * @copyright Copyright (c) 2024, 彭泽稳
*/
#ifndef _BSP_SWITCH_H_
#define _BSP_SWITCH_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "bsp_base.h"

/**
 * @brief: 读取开关量状态
 * @param id: [in], 开关量标识
 * @param status: [out], True: 高电平 False: 低电平
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
*/
Int32 Bsp_SwitchGetStatus(Int32 id, Bool* status);

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif // _BSP_SWITCH_H_
/**
 * @} 
 */
