/**
 * @file bsp_timer.h
 * @brief 板级支持包-定时器接口
 * @author 彭泽稳
 * @version v1.0
 * @par 修改历史
 * 版本     修改日期        修改人      修改内容
 * v1.0     2024-04-18      彭泽稳      第一版
 * @par 实例
 * Bsp_Init();
 * Bsp_Open();
 * 
 * /// 定时事件
 * void OnTimer(Bsp_THandle handle)
 * {
 *      Bool* flag = (Bool*)Bsp_TimerGetUserData(handle);
 *      *flag = True;
 * }
 * 
 * Bsp_THandle handle = Bsp_TimerCreate();
 * 
 * Bool g_flag = False;
 * Bsp_TimerSetUserData(handle, &g_flag);
 * Bsp_TimerSetEvent(handle, OnTimer);
 * 
 * Bsp_TimerStart(handle, 10);   ///< 定时10ms
 * 
 * /// RunOnce中判读标志
 * if (g_flag)  ///< 定时事件触发
 * {
 *      Bsp_TimerStop(handle);   ///< 停止定时器
 * }
 * 
 * Bsp_TimerFree(handle);
 *
 * Bsp_Close();
 * Bsp_Done();
 * 
 * @copyright Copyright (c) 2024, 彭泽稳
*/
#ifndef _BSP_TIMER_H_
#define _BSP_TIMER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "bsp_base.h"

/**
 * 定时器定时事件的回调函数
 * @param handle: 事件的产生者
 * @return 无返回值
 */
typedef void (* Bsp_TOnTimerEvent)(Bsp_THandle handle);

/**
 * 获取定时器节拍计数
 * @note 节拍计数值只有在[Bsp_Open](@ref Bsp_Open)打开后才会累加
 * @return 返回节拍计数值, 单位为毫秒(ms)
 */
UInt32 Bsp_TimerGetTick(void);

/**
 * 获取能够创建的定时器最大数量
 * @return 返回能够创建的定时器最大个数
 */
Int32 Bsp_TimerGetMaxCount(void);

/**
 * 获取当前能够创建的定时器数量
 * @return 返回当前能够创建的定时器个数
 */
Int32 Bsp_TimerGetCount(void);

/**
 * 创建定时器对象.
 * @note
 *   -# 当定时器个数超过Bsp_TimerGetMaxCount [@ Bsp_TimerGetMaxCount]时,创建失败
 * @return 如果创建失败返回NULL
 */
Bsp_THandle Bsp_TimerCreate(void);

/**
 * 释放定时器对象.
 * @param handle: [in], 句柄
 * @return 
 */
void Bsp_TimerFree(Bsp_THandle handle);

/**
 * 获取用户数据
 * @param handle: [in], 句柄
 * @return 返回用户数据
 */
void* Bsp_TimerGetUserData(Bsp_THandle handle);

/**
 * 设置用户数据
 * @param handle: 句柄
 * @param receiver: 用户数据
 * @return 无返回值
 */
void Bsp_TimerSetUserData(Bsp_THandle handle, void* user_data);

/**
 * 设置定时时间超时后触发的事件
 * @note 本函数实际在中断里面调用, 因此需要用户保持警惕, 切勿处理高延迟或不能在中断里面处理的操作
 * @param handle: [in], 句柄
 * @param on_event: [in], 事件方法
 * @return 无返回值
 */
void Bsp_TimerSetEvent(Bsp_THandle handle, Bsp_TOnTimerEvent on_event);

/**
 * 启动定时器
 * @note 启动后首次OnTimer事件会发生在定时间隔减一到定时间隔之间; \n
 *       如果定时间隔设置为1,则会在定时器使能时刻到1ms时触发OnTimer
 * @note 默认定时事件会循环触发，直到调用Bsp_TimerStop(@ Bsp_TimerStop)
 * @param handle: [in], 句柄
 * @param timeout, [in], 定时时间
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_TimerStart(Bsp_THandle handle, UInt32 timeout);

/**
 * 停止定时器
 * @param handle: [in], 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_TimerStop(Bsp_THandle handle);

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif // _BSP_TIMER_H_
/**
 * @} 
 */
