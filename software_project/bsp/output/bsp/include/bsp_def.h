#ifndef _BSP_DEF_H_
#define _BSP_DEF_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>

#include "bsp_axis_def.h"
#include "bsp_laser_def.h"
#include "bsp_led_def.h"
#include "bsp_misc_ctrl_def.h"
#include "bsp_motor_def.h"
#include "bsp_switch_def.h"
#include "bsp_uart_def.h"

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_DEF_H_ */
