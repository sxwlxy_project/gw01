/**
 * @file bsp_misc_ctrl.h
 * @brief 板级支持包-杂类控制信号接口
 * @author 彭泽稳
 * @version v1.0
 * @par 修改历史
 * 版本     修改日期        修改人      修改内容
 * v1.0     2024-04-18      彭泽稳      第一版
 * @par 实例
 * Bsp_Init();
 * Bsp_Open();
 * 
 * Bsp_MiscCtrlSetOn(0, True);   ///< 设置控制信号0高
 * Bsp_MiscCtrlSetOn(0, False);  ///< 设置控制信号0低
 * 
 * Bool status;
 * Bsp_MiscCtrlGetStatus(0, &status);    ///< 获取控制信号0状态
 * 
 * Bsp_Close();
 * Bsp_Done();
 * 
 * @copyright Copyright (c) 2024, 彭泽稳
*/
#ifndef _BSP_MISC_CTRL_H_
#define _BSP_MISC_CTRL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "bsp_base.h"

/**
 * @brief: 设置控制信号高低
 * @param id: [in], 标识
 * @param status: [in], True: 高电平， False: 低电平
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
*/
Int32 Bsp_MiscCtrlSetOn(Int32 id, Bool status);

/**
 * @brief: 读取控制信号状态
 * @param id: [in], 标识
 * @param status: [out], True: 高电平， False: 低电平
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
*/
Int32 Bsp_MiscCtrlGetStatus(Int32 id, Bool* status);

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif // _BSP_MISC_CTRL_H_
/**
 * @} 
 */
