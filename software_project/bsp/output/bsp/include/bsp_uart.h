/**
 * @file bsp_uart.h
 * @brief 板级支持包-串口接口
 * @author 彭泽稳
 * @version v1.0
 * @par 修改历史
 * 版本     修改日期        修改人      修改内容
 * v1.0     2024-04-18      彭泽稳      第一版
 * @par 实例
 * Bsp_Init();
 * Bsp_Open();
 * 
 * Int8 g_tx_buf[256];
 * Int8 g_rx_buf[256];
 * Bsp_THandle handle = Bsp_UartCreate(0, g_tx_buf, sizeof(g_tx_buf), g_rx_buf, sizeof(g_rx_buf));
 * 
 * 
 * Bsp_UartSetUserData(handle, );
 * 
 * Bsp_Close();
 * Bsp_Done();
 * 
 * @copyright Copyright (c) 2024, 彭泽稳
*/
#ifndef _BSP_UART_H_
#define _BSP_UART_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "bsp_base.h"

/// 奇偶校验类型定义
typedef enum Bsp_TUartParityTag Bsp_TUartParity;
enum Bsp_TUartParityTag
{
    Bsp_kUartParityNone         = 0,        ///< 无校验
    Bsp_kUartParityOdd          = 1,        ///< 奇校验
    Bsp_kUartParityEven         = 2,        ///< 偶校验
    Bsp_kUartParityMax
};

/**
 * 串口发送完成事件
 * @param handle: 事件的产生者
 * @return 无返回值
 */
typedef void (* Bsp_TUartOnSendEvent)(Bsp_THandle handle);

/**
 * 串口存在数据事件
 * @param handle: 事件的产生者
 * @return 无返回值
 */
typedef void (* Bsp_TUartOnRecvEvent)(Bsp_THandle handle);

/// 事件类型定义
typedef enum Bsp_TUartEventTag Bsp_TUartEvent;
enum Bsp_TUartEventTag
{
    Bsp_kUartEventOnSend        = 0,        ///< 串口发送完成事件
    Bsp_kUartEventOnRecv        = 1,        ///< 串口存在数据事件
    Bsp_kUartEventMax
};

/**
 * 创建串口对象.
 * @param id: [in], 串口标识
 * @param tx_buf: [in], 发送缓冲区, 提供给串口驱动内部使用
 * @param tx_buf_size: [in], 发送缓冲区大小
 * @param rx_buf: [in], 接收缓冲区, 提供给串口驱动内部使用
 * @param rx_buf_size: [in], 接收缓冲区大小
 * @return 如果创建失败返回NULL
 */
Bsp_THandle Bsp_UartCreate(Int32 id, Int8* tx_buf, UInt32 tx_buf_size, Int8* rx_buf, UInt32 rx_buf_size);

/**
 * 释放串口对象.
 * @param handle: [in], 句柄
 * @return 
 */
void Bsp_UartFree(Bsp_THandle handle);

/**
 * 获取串口标识
 * @param handle: [in], 句柄
 * @return 返回用于创建句柄的标识, 参见Bsp_TUartId(@ bsp_uart_def.h)
 */
Int32 Bsp_UartGetId(Bsp_THandle handle);

/**
 * 获取用户数据
 * @param handle: [in], 句柄
 * @return 返回用户数据
 */
void* Bsp_UartGetUserData(Bsp_THandle handle);

/**
 * 设置用户数据
 * @param handle: [in], 句柄
 * @param user_data: [in], 用户数据
 * @return 无返回值
 */
void Bsp_UartSetUserData(Bsp_THandle handle, void *user_data);

/**
 * 设置事件
 * @param handle: 句柄
 * @param which_event: [in], 选择设置哪个事件, 参见Bsp_TUartEvent(@ bsp_uart.h)
 * @param on_event: [in], 事件函数, 具体类型参见不同事件的事件函数定义
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_UartSetEvent(Bsp_THandle handle, Bsp_TUartEvent which_event, void* on_event);

/**
 * 打开设备
 * @param handle: [in], 句柄
 * @param baudrate: [in], 波特率,如115200, 9600
 * @param parity: [in], 奇偶位类型, 参见Bsp_TUartParity(@ Bsp_TUartParityTag)
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_UartOpen(Bsp_THandle handle, Int32 baudrate, Int32 parity);

/**
 * 关闭设备
 * @param handle: 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_UartClose(Bsp_THandle handle);

/**
 * 获取当前的波特率
 * @param handle: [in], 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_UartGetBaudrate(Bsp_THandle handle);

/**
 * 返回当前串口是否已经打开
 * @param handle: [in], 句柄
 * @return 返回 True, 为已打开; 返回 False, 为未打开
 */
Bool Bsp_UartIsOpen(Bsp_THandle handle);

/**
 * 驱动读取数据
 * @note 此函数不阻塞
 * @param handle: [in], 句柄
 * @param buf: [out], 数据缓存
 * @param size: [in], 缓冲区长度
 * @return 当返回值 >= 0 时, 表示成功并返回实际读取的个数
 *         当返回值 < 0 时, 表示失败参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_UartRead(Bsp_THandle handle, void *buf, UInt32 size);

/**
 * 驱动写入数据
 * @note 此函数不阻塞
 * @param handle: [in], 句柄
 * @param data: [in], 数据指针
 * @param size: [in], 数据长度
 * @return 当返回值 >= 0 时, 表示成功并返回实际发送的个数
 *         当返回值 < 0 时, 表示失败参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_UartWrite(Bsp_THandle handle, const void *data, UInt32 size);

/**
 * 返回当前串口是否有数据可以读取
 * @param handle: [in], 句柄
 * @return >= 0, 表示可以读取的数据个数, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_UartReadableCount(Bsp_THandle handle);

/**
 * 返回当前串口是否可以写入
 * @param handle: [in], 句柄
 * @return >= 0, 表示可以写入的数据个数, <0 表示失败, 参见Bsp_TErrorCode(@ Bsp_TErrorCode)
 */
Int32 Bsp_UartWritableCount(Bsp_THandle handle);

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif // _BSP_UART_H_
/**
 * @} 
 */
