#ifndef _BSP_MOTOR_DEF_H_
#define _BSP_MOTOR_DEF_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>

/// 电机标识
typedef enum Bsp_TMotorIdTag Bsp_TMotorId;
enum Bsp_TMotorIdTag
{
    Bsp_kMotorIdBase                    = 0,
    Bsp_kMotorIdFrontLeft               = Bsp_kMotorIdBase,     ///< 前左电机
    Bsp_kMotorIdFrontRight              = 1,                    ///< 前右电机
    Bsp_kMotorIdRearLeft                = 2,                    ///< 后左电机
    Bsp_kMotorIdRearRight               = 3,                    ///< 后右电机
    Bsp_kMotorIdMax,
};

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_MOTOR_DEF_H_ */
