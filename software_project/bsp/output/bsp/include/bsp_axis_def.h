#ifndef _BSP_AXIS_DEF_H_
#define _BSP_AXIS_DEF_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "BaseTypes.h"

/// 轴加速度数据结构
typedef struct Bsp_TAxisAcceDataTag Bsp_TAxisAcceData;
struct Bsp_TAxisAcceDataTag
{
    float ax;   ///< x方向加速度, 单位mg
    float ay;   ///< y方向加速度, 单位mg
    float az;   ///< z方向加速度, 单位mg
};

/// 轴角速度数据结构
typedef struct Bsp_TAxisGyroDataTag Bsp_TAxisGyroData;
struct Bsp_TAxisGyroDataTag
{
    float gx;   ///< x方向角速度, 单位度/s
    float gy;   ///< y方向角速度, 单位度/s
    float gz;   ///< z方向角速度, 单位度/s
};

/// 轴角度数据结构
typedef struct Bsp_TAxisAngleDataTag Bsp_TAxisAngleData;
struct Bsp_TAxisAngleDataTag
{
    float roll;     ///< 横滚角, 单位度
    float pitch;    ///< 俯仰角, 单位度
    float yaw;      ///< 航向角, 单位度
};

/// 轴数据类型定义
typedef enum Bsp_TAxisDataTypeTag Bsp_TAxisDataType;
enum Bsp_TAxisDataTypeTag
{
    Bsp_kAxisDataTypeBase       = 0,
    Bsp_kAxisDataTypeAcce       = Bsp_kAxisDataTypeBase,
    Bsp_kAxisDataTypeGyro       = 1,
    Bsp_kAxisDataTypeAngle      = 2,
    Bsp_kAxisDataTypeMax,
};

/// 轴ID定义
typedef enum Bsp_TAxisIdTag Bsp_TAxisId;
enum Bsp_TAxisIdTag
{
    Bsp_kAxisIdBase         = 0,
    Bsp_kAxisIdWt61c        = Bsp_kAxisIdBase,      ///< WT61C
    Bsp_kAxisIdMax,
};

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif /** _BSP_AXIS_DEF_H_ */
