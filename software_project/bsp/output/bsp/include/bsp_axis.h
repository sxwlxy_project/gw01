/**
 * @file bsp_axis.h
 * @brief 板级支持包-轴传感器接口
 * @author 彭泽稳
 * @version v1.0
 * @par 修改历史
 * 版本     修改日期        修改人      修改内容
 * v1.0     2024-04-17      彭泽稳      第一版
 * @par 实例
 * Bsp_Init();
 * Bsp_Open();
 * 
 * Bsp_THandle handle = Bsp_AxisCreate(0);
 * 
 * /// 设置轴数据缓存, 并打开轴
 * Bsp_TAxisAcceData acce_data[10];
 * Bsp_AxisSetDataDepth(handle, Bsp_kAxisDataTypeAcce, acce_data, sizeof(acce_data) / sizeof(acce_data[0]));
 * Bsp_AxisOpen(handle);
 * 
 * /// 校准轴
 * Bsp_AxisCalibrate(handle);   
 * 
 * // 线程中 
 * Bsp_AxisRunOnce(handle);
 * 
 * /// 获取轴数据
 * Bsp_TAxisAcceData read_data;
 * Int32 result = Bsp_AxisGetData(handle, Bsp_kAxisDataTypeAcce, read_data, 1);
 * if (result > 0)
 * {
 *     /// 获取数据成功
 * }
 * 
 * Bsp_AxisClose(handle);
 * Bsp_AxisFree(handle);
 * 
 * Bsp_Close();
 * Bsp_Done();
 * 
 * @copyright Copyright (c) 2024, 彭泽稳
*/
#ifndef _BSP_AXIS_H_
#define _BSP_AXIS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "BaseTypes.h"
#include "bsp_base.h"
#include "bsp_axis_def.h"

/**
 * 创建轴对象.
 * @param id: [in], 轴标识, 参见Bsp_TAxisId(@ bsp_axis_def.h)
 * @return 如果创建失败返回NULL
 */
Bsp_THandle Bsp_AxisCreate(Int32 id);

/**
 * 销毁轴对象.
 * @param handle: [in], 句柄
 * @return 
 */
void Bsp_AxisFree(Bsp_THandle handle);

/**
 * 打开轴对象.
 * @note 用户必须在调用此函数之前调用 Bsp_AxisSetDataDepth 函数设置轴数据的深度
 * @param handle: [in], 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
 */
Int32 Bsp_AxisOpen(Bsp_THandle handle);

/**
 * 关闭轴对象.
 * @param handle: [in], 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
 */
Int32 Bsp_AxisClose(Bsp_THandle handle);

/**
 * 设置轴数据深度.
 * @note 用户必须保证在打开轴对象之前调用该函数, 否则无效
 * @param handle: [in], 句柄
 * @param data_type: [in], 轴数据的类型, 参见Bsp_TAxisDataType(@ bsp_axis_def.h)
 * @param data: [in], 提供给轴对象的缓存, 用于内部缓存对应类型的数据
 * @param depth: [in], 数据的深度, 内部最大存储depth个的数据, 用户也最多取出depth个的数据
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
 */
Int32 Bsp_AxisSetDataDepth(Bsp_THandle handle, Int8 data_type, void* data, Int32 depth);

/**
 * 获取轴数据.
 * @param handle: [in], 句柄
 * @param data_type: [in], 轴数据的类型, 参见Bsp_TAxisDataType(@ bsp_axis_def.h)
 * @param data: [out], 用于接收获取到的轴数据
 * @param depth: [in], 尝试获取轴数据的个数
 * @return >=0 表示实际获取到的个数, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
 */
Int32 Bsp_AxisGetData(Bsp_THandle handle, Int8 data_type, void* data, Int32 count);

/**
 * 校准轴.
 * @param handle: [in], 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
 */
Int32 Bsp_AxisCalibrate(Bsp_THandle handle);

/**
 * 轴传感器模块功能调度一次
 * @note 用户需要在线程中周期调度此函数, 保证轴模块能够正常工作
 * @param handle: [in], 句柄
 * @return >=0 表示成功, <0 表示失败, 参见Bsp_TErrorCode(@ bsp_error.h)
*/
Int32 Bsp_AxisRunOnce(Bsp_THandle handle);

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif /** _BSP_AXIS_H_ */
