#ifndef _BSP_SWITCH_DEF_H_
#define _BSP_SWITCH_DEF_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <BaseTypes.h>

/// 开关量标识
typedef enum Bsp_TSwitchIdTag Bsp_TSwitchId;
enum Bsp_TSwitchIdTag
{
    Bsp_kSwitchIdBase           = 0,
    Bsp_kSwitchIdPb5            = Bsp_kSwitchIdBase,    ///< PB5
    Bsp_kSwitchIdPb6            = 1,                    ///< PB6
    Bsp_kSwitchIdPb7            = 2,                    ///< PB7
    Bsp_kSwitchIdPb8            = 3,                    ///< PB8
    Bsp_kSwitchIdPb9            = 4,                    ///< PB9
    Bsp_kSwitchIdMax,
};

#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* _BSP_SWITCH_DEF_H_ */
