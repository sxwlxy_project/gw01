#include "bsp_inc.h"
//#include "osc_api.h"
//#include "osc_task.h"

void LaserDemo_Init(void);
void AxisDemo_Init(void);
void UartDemo_Init(void);
void MotorDemo_Init(void);
void SwitchDemo_Init(void);
void MiscCtrlDemo_Init(void);

void Delay(UInt32 ms)
{
    // volatile UInt32 g_led_timeout = 72000;
    // while (ms-- > 0)
    // {
    //     g_led_timeout = 7200;
    //     while(g_led_timeout-- > 0);
    // }

    UInt32 tick = Bsp_TimerGetTick();
    UInt32 cur_tick = tick;

    while (cur_tick - tick < ms)
        cur_tick = Bsp_TimerGetTick();
}

UInt32 g_count;

void Test_OnTimer(Bsp_THandle handle)
{
    Bool* on = (Bool*)Bsp_TimerGetUserData(handle);
    Bsp_LedSetOn(Bsp_kLedId0, *on);
    *on = !(*on);

    g_count++;
    if (g_count >= 10)
        Bsp_TimerStop(handle);
}

int main(void)
{
    volatile Int32 result;
    volatile Bsp_THandle handle;

    Bsp_Init();
    Bsp_Open();

//    do
//    {
//        Delay(500);
//        Bsp_LedSetOn(Bsp_kLedId0, True);
//        Delay(500);
//        Bsp_LedSetOn(Bsp_kLedId0, False);
//    } while (0);

    Bsp_Close();
    Bsp_Open();
    
//    result = Bsp_TimerGetMaxCount();
//    result = Bsp_TimerGetCount();
//    handle = Bsp_TimerCreate();
//    
//    Bool on = True;
//    Bsp_TimerSetUserData(handle, &on);
//    Bsp_TimerSetEvent(handle, Test_OnTimer);
//    Bsp_TimerStart(handle, 500);

//    while (g_count < 10);
//    
//    Bsp_TimerFree(handle);

//    UartDemo_Init();
    //MotorDemo_Init();
    //SwitchDemo_Init();
    LaserDemo_Init();

    Bsp_Close();
    Bsp_Done();

    return 0;
}
