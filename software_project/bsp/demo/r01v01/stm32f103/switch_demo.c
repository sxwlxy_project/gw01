#include "bsp_inc.h"

extern void Delay(UInt32 ms);

static Bool status;
static volatile Int32 result;

void SwitchDemo_Init(void)
{
    result = Bsp_SwitchGetStatus(Bsp_kSwitchIdPb5, &status);
    result = Bsp_SwitchGetStatus(Bsp_kSwitchIdPb6, &status);
    result = Bsp_SwitchGetStatus(Bsp_kSwitchIdPb7, &status);
    result = Bsp_SwitchGetStatus(Bsp_kSwitchIdPb8, &status);
    result = Bsp_SwitchGetStatus(Bsp_kSwitchIdPb9, &status);

}
