#include "bsp_inc.h"

extern void Delay(UInt32 ms);

static Bool status;
static volatile Int32 result;

void MiscCtrlDemo_Init(void)
{
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd0, True);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd1, True);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd2, True);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd3, True);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd4, True);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd5, True);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe10, True);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe11, True);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe12, True);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe13, True);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe14, True);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe15, True);

    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd0, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd1, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd2, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd3, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd4, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd5, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe10, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe11, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe12, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe13, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe14, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe15, &status);

    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd0, False);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd1, False);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd2, False);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd3, False);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd4, False);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPd5, False);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe10, False);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe11, False);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe12, False);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe13, False);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe14, False);
    result = Bsp_MiscCtrlSetOn(Bsp_kMiscCtrlIdPe15, False);

    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd0, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd1, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd2, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd3, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd4, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPd5, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe10, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe11, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe12, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe13, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe14, &status);
    result = Bsp_MiscCtrlGetStatus(Bsp_kMiscCtrlIdPe15, &status);

}
