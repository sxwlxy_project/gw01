#include "bsp_inc.h"
#include "osc_api.h"
#include "osc_task.h"
#include "osc_application.h"

typedef struct LaserDemo_TAttrTag   LaserDemo_TAttr;
struct LaserDemo_TAttrTag
{
    Bsp_THandle handle;
    Bsp_TLaserData data[10];
};

static LaserDemo_TAttr g_laser_demo_attr_;

static Osc_TTask g_laser_demo_task_ = {0};
static Int8 g_laser_demo_task_stack_mem_[512] __attribute__((aligned(8))) = {0};
static const Osc_TTaskAttr g_laser_demo_task_attr_ = {
    .name = "LaserTask",
    .priority = Osc_kTaskPriorityNormal,
    .stack_mem = g_laser_demo_task_stack_mem_,
    .stack_size = sizeof(g_laser_demo_task_stack_mem_),
};

static void LaserDemo_TaskOnStart(Osc_TTask *self)
{
    LaserDemo_TAttr* attr = &g_laser_demo_attr_;

    attr->handle = Bsp_LaserCreate(0);
    Bsp_LaserSetDataDepth(attr->handle, 0, attr->data, sizeof(attr->data) / sizeof(attr->data[0]));
    Bsp_LaserOpen(attr->handle);
}

static void LaserDemo_TaskOnStop(Osc_TTask *self)
{
    LaserDemo_TAttr* attr = &g_laser_demo_attr_;

    Bsp_LaserClose(attr->handle);
    Bsp_LaserFree(attr->handle);
}

    Bsp_TLaserData data[10];
    
static UInt32 LaserDemo_TaskOnRunOnce(Osc_TTask *self)
{
    LaserDemo_TAttr* attr = &g_laser_demo_attr_;
    volatile Int32 count;

    Bsp_LaserRunOnce(attr->handle);
    
    Bsp_LaserGetData(attr->handle, 0, data, sizeof(data) / sizeof(data[0]));
    
    return 50;
}

void LaserDemo_Init(void)
{
    Osc_TApplicationInitialize(&g_osc_application);
    
    if (Osc_TTaskCreate(&g_laser_demo_task_, &g_laser_demo_task_attr_) >= 0)
    {
        g_laser_demo_task_.OnStart = LaserDemo_TaskOnStart;
        g_laser_demo_task_.OnRunOnce = LaserDemo_TaskOnRunOnce;
        g_laser_demo_task_.OnTerminate = LaserDemo_TaskOnStop;

        Osc_TApplicationSetMainTask(&g_osc_application, &g_laser_demo_task_);
        Osc_TApplicationRun(&g_osc_application);
    }

    while (1);

}
