/**
 * @addtogroup abc_data_coder
 * abc_data_coder
 * @{
 */
/**
 * @file        abc_data_coder.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/03/18
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/03/18   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */

#ifndef _ABC_DATA_CODER_H_
#define _ABC_DATA_CODER_H_

#include "abc/"

#ifdef __cplusplus
extern "C"
{
#endif






#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _ABC_DATA_CODER_H_

/**
 * @}  Generated on "2024-03-18 00:10:50" by the tool "gen_hq_file.py >> V20231119" 
 */

