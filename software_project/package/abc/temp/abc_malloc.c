/**
 * @file        abc_malloc.c
 * @brief       动态内存管理
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/03/16
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/03/16   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */
 
#include "abc_malloc.h"

typedef struct Abc_TMallocMgrTag Abc_TMallocMgr;
struct Abc_TMallocMgrTag
{
    Bool is_init;
    void *memory_addr;
    UInt32 memory_size;
};


static Abc_TMallocMgr g_malloc_mgr_ = {0};



Int32 Abc_MallocInit(void *addr, UInt32 size)
{
    Int32 result = False;

    if (g_malloc_mgr_.is_init)
        result = 1;
    else if (addr == NULL || size < 1024/*最小值*/)
        result = -1;
    else
    {
        g_malloc_mgr_.memory_addr = addr;
        g_malloc_mgr_.memory_size = size;
        g_malloc_mgr_.is_init = True;
        result = 0;
    }

    return result;
}


void Abc_MallocDone(void)
{
    if (g_malloc_mgr_.is_init)
    {
        g_malloc_mgr_.is_init = False;
    }
}


void *Abc_Malloc(UInt32 size)
{
    void *result = NULL;

    return result;
}


void Abc_Free(void *ptr)
{
    
}


