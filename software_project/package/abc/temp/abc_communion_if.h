/**
 * @addtogroup abc_communion_if
 * abc_communion_if
 * @{
 */
/**
 * @file        abc_communion_if.h
 * @brief       数据交互接口
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/03/17
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/03/17   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 
 */

#ifndef _ABC_COMMUNION_IF_H_
#define _ABC_COMMUNION_IF_H_

#include "abc/abc_types.h"

#ifdef __cplusplus
extern "C"
{
#endif


typedef struct Abc_IDataStreamCommunionTag Abc_IDataStreamCommunion;

typedef Int32 (* Abc_IDSCommunionSendData)(Abc_IDataStreamCommunion *sender, const Int8 *data, Int32 size);


typedef void (* Abc_IDSCommunionSendConfirmCallback)(Abc_IDataStreamCommunion *sender, Bool result);


typedef Int32 (* Abc_IDSCommunionRecvData)(Abc_IDataStreamCommunion *sender, Int8 *data, Int32 size);


typedef void (* Abc_IDSCommunionRecvInformCallback)(Abc_IDataStreamCommunion *sender, const Int8 *data, Int32 size);


struct Abc_IDataStreamCommunionTag
{
    // 主动发送数据
    Abc_IDSCommunionSendData DoSendData;

    // 发送结果确认
    Abc_IDSCommunionSendConfirmCallback OnSendConfirm;

    // 数据读取方法
    Abc_IDSCommunionRecvData DoRecvData;
    
    // 数据接收通知
    Abc_IDSCommunionRecvInformCallback OnRecvInform;
};



Int32 Abc_IDataStreamCommunionSendData(Abc_IDataStreamCommunion *sender, const Int8 *data, Int32 size);


void Abc_IDataStreamCommunionSendConfirmCallback(Abc_IDataStreamCommunion *sender, Bool result);


Int32 Abc_IDataStreamCommunionRecvData(Abc_IDataStreamCommunion *sender, Int8 *data, Int32 size);


void Abc_IDataStreamCommunionRecvInformCallback(Abc_IDataStreamCommunion *sender, const Int8 *data, Int32 size);



#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _ABC_COMMUNION_IF_H_

/**
 * @}  Generated on "2024-03-17 23:20:12" by the tool "gen_hq_file.py >> V20231119" 
 */

