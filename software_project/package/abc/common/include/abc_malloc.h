/**
 * @addtogroup abc_malloc
 * abc_malloc
 * @{
 */
/**
 * @file        abc_malloc.h
 * @brief       动态内存管理
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/03/16
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/03/16   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright xxx
 */

#ifndef _ABC_MALLOC_H_
#define _ABC_MALLOC_H_

#include "abc/abc_types.h"


#ifndef Malloc(size)
#define Malloc(size)  Abc_Malloc(size)
#endif

#ifndef Free(ptr)
#define Free(ptr)  Abc_Free(ptr)
#endif


#ifdef __cplusplus
extern "C"
{
#endif


/**
 * 动态内存功能初始化
 * @param *addr 用于动态分配的内存地址
 * @param size  用于动态分配的内存大小
 * @return >= 0 成功
 *         <    失败
 */
Int32 Abc_MallocInit(void *addr, UInt32 size);

/**
 * 动态内存功能反初始化
 * @return 
 */
void Abc_MallocDone(void);

/**
 * 动态申请内存
 * @param size  申请内存大小
 * @return != NULL  成功，可用的内存起始地址
 *         == NULL  失败
 */
void *Abc_Malloc(UInt32 size);

/**
 * 释放动态申请的内存
 * @param *ptr
 * @return 
 */
void Abc_Free(void *ptr);



#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _ABC_MALLOC_H_


