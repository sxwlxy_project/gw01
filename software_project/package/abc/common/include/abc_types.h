// =======================================
// Unit   : base types (基础类型)
// Version: 
// Author : Auyv
// Email  : 
// =======================================

#ifndef _ABC_TYPES_H_
#define _ABC_TYPES_H_

#ifndef _BaseTypes_H_
#define _BaseTypes_H_


#ifdef __cplusplus
extern "C"
{
#endif

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/* 非 Windows 下无 __stdcall/__cdecl 修饰符, 为了兼容 Windows 下修饰符 */

#if !defined(_WIN32) && !defined(_WIN64) && !defined(_WINDOWS) && !defined(WINCE)
   #ifndef __stdcall    // __stdcall 宏定义
   #define __stdcall
   #endif

   #ifndef __cdecl      // __cdecl 宏定义
   #define __cdecl
   #endif
#endif

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/* 常量列表 */

// 空指针值
#ifndef NULL
#define NULL            0
#endif

// 取结构项偏移量的宏{T: 结构类型, I: 结构项}
#define  __Offset__(T, I)  ((long)&((T*)0)->I)

// 取绝对值的宏
#ifndef  ABS
#define  ABS(a)         (((a) >= 0)   ? (a) : -(a))
#endif

// 取最小值的宏
#ifndef  MIN
#define  MIN(a, b)      (((a) <= (b)) ? (a) : (b))
#endif

// 取最大值的宏
#ifndef  MAX
#define  MAX(a, b)      (((a) >= (b)) ? (a) : (b))
#endif


// 取合法范围值
#ifndef  CLAMP
#define CLAMP(value, min, max) ((value) < (min) ? (min) : ((value) > (max) ? (max) : (value)))
#endif

// 常量值
enum _ABC_TYPES_const_
{
   // 布尔值
   False                = 0,              // 布尔值假
   True                 = 1,              // 布尔值真

   // ... ...
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/* 基本数据类型定义 */

// 64位整型
#if defined(__LP64__)
   typedef long               TInt64;
   typedef unsigned long      TUInt64;
#elif defined(_MSC_VER)
   typedef __int64            TInt64;
   typedef unsigned __int64   TUInt64;
#else
   typedef long long int      TInt64;
   typedef unsigned long long TUInt64;
#endif



// 有符号整型
typedef TInt64             Int64,      *PInt64;
typedef signed int         Int32,      *PInt32;
typedef signed short       Int16,      *PInt16;
typedef signed char        Int8,       *PInt8;
typedef signed int         Integer,    *PInteger;
typedef signed long        Longint,    *PLongint,  Long;
typedef signed short       Smallint,   *PSmallint;
typedef signed char        Shortint,   *PShortint;

// 无符号整型
typedef TUInt64            UInt64,     *PUInt64;
typedef unsigned int       UInt32,     *PUInt32;
typedef unsigned short     UInt16,     *PUInt16;
typedef unsigned char      UInt8,      *PUInt8;
typedef unsigned long      Longword,   *PLongword;
typedef TUInt64            QWord,      *PQWord;
typedef unsigned int       DWord,      *PDWord;
typedef unsigned short     Word,       *PWord;
typedef unsigned char      Byte,       *PByte;

// 布尔类型
typedef unsigned char      Bool;

// 字符串指针
typedef char*              PChar;

// 无类型指针类型
typedef void*              Pointer;
typedef void**             PPointer;

#ifdef __cplusplus
}  // extern "C"
#endif

#endif

#endif

