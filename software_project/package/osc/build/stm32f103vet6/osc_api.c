/**
 * 
 * @File    : 
 * @Brief   : ... 
 * @Version : 1.0
 * @Author  : 靳普诏
 * @E-Mail  : 499367619@qq.com
 */

#include "osc_api.h"

#include "cmsis_os2.h"
#include "os_tick.h"

/**
 * 获取操作系统信息
 * @param ret_info 返回相关字符串信息
 * @return 关于版本整型值
 */
Int32 Osc_Version(PChar ret_info)
{
    if (ret_info != NULL)
        ret_info = (PChar)"Osc_Version:1.0";
    
    return 1;
}


/**
 * 获取操作系统当前Tick值
 * @return 当前Tick值
 */
UInt32 Osc_SystemTick(void)
{
    return (UInt32)osKernelGetTickCount();
}

/**
 * 睡眠 毫秒ms
 * @param ms
 * @return 
 */
Bool Osc_SleepMs(UInt32 ms)
{
    UInt32 tick = osKernelGetTickCount();
    osDelayUntil(tick + ms);
    return True;
}


///**
// * 睡眠 微秒us
// * @param us
// * @return 
// */
//void Osc_SleepUs(UInt32 us)
//{

//}

