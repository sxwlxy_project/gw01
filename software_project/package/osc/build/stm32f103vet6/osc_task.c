/**
 * @File    : Osc_task.h
 * @Brief   : 线程(任务)对象
 * @Version : 1.0
 * @Author  : 靳普诏
 * @E-Mail  : 499367619@qq.com
 */


#include "osc_task.h"

#include "cmsis_os2.h"

//typedef union 
//{
//    /* data */
//} _TTask;


static void L_OSC_TaskOnRun(Osc_TTask *self) 
{
    Osc_TTaskOnRunOnceMethod method;
    Osc_TTaskOnStateChangeMethod state_change_method;

    state_change_method = self->OnStart;
    if (state_change_method != NULL)
        state_change_method(self);

    while (!Osc_TTaskIsTerminated(self))
    {
        method = self->OnRunOnce;

        if (method != NULL)
        {
            UInt32 sleep_ms = method(self);
            Osc_SleepMs(sleep_ms);
        }
        else
            Osc_SleepMs(1000);
    }
    
    
    state_change_method = self->OnTerminate;
    if (state_change_method != NULL)
        state_change_method(self);

    osThreadExit();
}


/**
 * 任务创建
 * @param *self         任务句柄
 * @param *attr         任务属性
 * @param run_method    任务方法
 * @return >= 0 成功
 *         <  0 失败
 */
Int32 Osc_TTaskCreate(Osc_TTask *self, const Osc_TTaskAttr *attr)
{
    Int32 result = -1;
    
    if (self != NULL && attr != NULL)
    {
        osThreadAttr_t thread1_attr = 
        {
            .name = attr->name,
            .priority = (attr->priority + 1) * 8, // 转换成 osPriority_t
            .stack_mem = attr->stack_mem,                          
            .stack_size = attr->stack_size,                         
        };
        
        self->handle_ = osThreadNew((osThreadFunc_t)L_OSC_TaskOnRun, self, &thread1_attr);    // Create thread with custom sized stack memory

        if (self->handle_ !=NULL)
        {
            result = 0;    // 创建成功
        }
        else
        {
            result = -2;    // 创建失败
        }
        
    }
    
    return result;
}


/**
 * 任务销毁
 * @param *self         任务句柄
 * @return 
 */
void Osc_TTaskDestroy(Osc_TTask *self)
{
    if (self != NULL && self->handle_ != NULL)
    {
        Osc_TTaskTerminate(self);
        
        osThreadJoin(self->handle_);
        osThreadDetach(self->handle_);
    }
}


/**
 * 开始任务
 * @param *self
 * @return 
 */
Int32 Osc_TTaskStart(Osc_TTask *self)
{
    Int32 result = -1;
    
    if (osThreadResume(self->handle_) == osOK)
    {
        result = 0;
    }
    
    return result;
}

/**
 * 任务正在执行
 * @param *self
 * @return 
 */
Bool Osc_TTaskIsTerminated(Osc_TTask *self)
{
    osThreadState_t state = osThreadGetState(self->handle_);
    return state == osThreadTerminated;
}


/**
 * 停止任务
 * @param *self
 * @return 
 */
void Osc_TTaskTerminate(Osc_TTask *self)
{
    osThreadTerminate(self->handle_);
}
