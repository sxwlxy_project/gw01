/**
 * @file        Osc_application.c
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/05
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/05   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */
 
#include "osc_application.h"

#include "cmsis_os2.h"

#include "stm32f10x.h"
#include "RTE_Components.h"
#include "os_tick.h"
#include "RTX_Config.h"

Osc_TApplication g_osc_application;

#ifndef SYSTICK_IRQ_PRIORITY
#define SYSTICK_IRQ_PRIORITY    0xFFU
#endif


int32_t OS_Tick_Setup (uint32_t freq, IRQHandler_t handler)
{
  uint32_t load;
  (void)handler;

  if (freq == 0U) {
    //lint -e{904} "Return statement before end of function"
    return (-1);
  }

  load = (SystemCoreClock / freq);
//  load = 0x00FFFFFFU - (SystemCoreClock / OS_TICK_FREQ);
  if (load > 0x00FFFFFFU) {
    //lint -e{904} "Return statement before end of function"
    return (-1);
  }

  // Set SysTick Interrupt Priority
#if   ((defined(__ARM_ARCH_8M_MAIN__)   && (__ARM_ARCH_8M_MAIN__   != 0)) || \
       (defined(__ARM_ARCH_8_1M_MAIN__) && (__ARM_ARCH_8_1M_MAIN__ != 0)) || \
       (defined(__CORTEX_M)             && (__CORTEX_M             == 7U)))
  SCB->SHPR[11] = SYSTICK_IRQ_PRIORITY;
#elif  (defined(__ARM_ARCH_8M_BASE__)   && (__ARM_ARCH_8M_BASE__   != 0))
  SCB->SHPR[1] |= ((uint32_t)SYSTICK_IRQ_PRIORITY << 24);
#elif ((defined(__ARM_ARCH_7M__)        && (__ARM_ARCH_7M__        != 0)) || \
       (defined(__ARM_ARCH_7EM__)       && (__ARM_ARCH_7EM__       != 0)))
  SCB->SHP[11]  = SYSTICK_IRQ_PRIORITY;
#elif  (defined(__ARM_ARCH_6M__)        && (__ARM_ARCH_6M__        != 0))
  SCB->SHP[1]  |= ((uint32_t)SYSTICK_IRQ_PRIORITY << 24);
#else
#error "Unknown ARM Core!"
#endif

  SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_TICKINT_Msk;
  SysTick->LOAD =  load;
  SysTick->VAL  =  0U;

  return (0);
}
//{
//    // 首先，设置SysTick的重载值，以产生1ms的中断间隔
//    // SysTick的计数器是24位的，所以最大值是0xFFFFFF
//    // 系统时钟为8MHz，要得到1ms的中断间隔，计算如下：
//    // 计数间隔 = 系统时钟频率 / 1000 (1ms)
//    // 重载值 = 0xFFFFFF - 计数间隔
//    uint32_t reloadValue = 0xFFFFFF - (8000000 / 1000);
//    SysTick->LOAD = reloadValue; // 设置重载值

//    // 设置SysTick的计数器的当前值，这个值可以是任意的，但通常设置为0
//    SysTick->VAL = 0;

//    // 设置SysTick的控制寄存器，使能中断并启动计数器
//    // 选择时钟源为HCLK，使能计数器，使能中断
//    SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_TICKINT_Msk;
//}



/**
 * 应用框架初始化
 * @note 主任务必须在调用完这个函数后创建
 * @param self: 对象实例
 * @return 无返回值
 */
void Osc_TApplicationInitialize(Osc_TApplication *self)
{
//    L_Gw01_Car_SysTick_Init();
    // 初始化应用框架
    osKernelInitialize();
}

/**
 * 获取应用框架的主任务
 * @param self: 对象实例
 * @return 返回指向主任务的对象
 */
Osc_TTask *Osc_TApplicationMainTask(Osc_TApplication *self)
{
    return self != NULL ? self->task_ : NULL;
}

/**
 * 设置应用框架的主任务.
 * @note 这个函数必须在[Osc_TApplicationInitialize](@ref Osc_TApplicationInitialize)之后,[Osc_TApplicationRun](@ref Osc_TApplicationRun)前调用
 * @param self: 对象实例
 * @param task: 创建的主任务 
 * @return 无返回值
 */
void Osc_TApplicationSetMainTask(Osc_TApplication *self, Osc_TTask *task)
{
    if (self != NULL)
    {
        self->task_ = task;
    }
}

/**
 * 应用框架运行
 * @note 调用前需要创建一个主任务
 * @param self: 对象实例
 * @return 无返回值
 */
void Osc_TApplicationRun(Osc_TApplication *self)
{
    osKernelStart();
}





///< Generated on "2024-04-05 23:35:20" by the tool "gen_hq_file.py >> V20231119" 

