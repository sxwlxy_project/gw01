/**
 * @addtogroup bot_motor
 * bot_motor
 * @{
 */
/**
 * @file        bot_motor.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(499367619@qq.com)
 * @date        2024/04/01
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/01   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 
 */

#ifndef _BOT_MOTOR_H_
#define _BOT_MOTOR_H_






#ifdef __cplusplus
extern "C"
{
#endif






#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _BOT_MOTOR_H_

/**
 * @}  Generated on "2024-04-01 22:58:08" by the tool "gen_hq_file.py >> V20231119" 
 */

