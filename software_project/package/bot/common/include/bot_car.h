/**
 * @addtogroup bot_car
 * bot_car
 * @{
 */
/**
 * @file        bot_car.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/02
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/02   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 
 */

#ifndef _BOT_CAR_H_
#define _BOT_CAR_H_

#include "abc_types.h"

#ifdef __cplusplus
extern "C"
{
#endif
enum Bot_TCarStateMaskTag
{
    Bot_kCarStateMaskValid = (0x1 << 0),
    Bot_kCarStateMaskArrived = (0x1 << 1),
    Bot_kCarStateMaskPlaceMod = (0x1 << 2),
};


typedef struct Bot_TCarPlaceTag Bot_TCarPlace;
struct Bot_TCarPlaceTag
{
    Int32 x;    // x 横向坐标                           // y ^
    Int32 y;    // y 纵向坐标                           //   |
                                                        //   |
    Int32 d;    // 航向角. 单位: 0.001度. x轴为 0度     //   ------->
                                                        //          x
};



typedef struct Bot_TCarTag Bot_TCar;

typedef void (* Bot_TCarDoStateNotifyMethod)(Bot_TCar *self, UInt32 state);
typedef Bool (* Bot_TCarDoGetCurrPlaceMethod)(Bot_TCar *self, Bot_TCarPlace *ret_place);
typedef Bool (* Bot_TCarDoSetCurrPlaceMethod)(Bot_TCar *self, const Bot_TCarPlace *place);
typedef Bool (* Bot_TCarDoSetMotivePwrMethod)(Bot_TCar *self, Int32 angle, Int32 distance);
typedef Bool (* Bot_TCarDoSetSpinPwrMethod)(Bot_TCar *self, Int32 diff_angle);
typedef void (* Bot_TCarDoRunOnceMethod)(Bot_TCar *self);
typedef Bool (* Bot_TCarDoCheckPlaceValidMethod)(Bot_TCar *self, const Bot_TCarPlace *in_place);

struct Bot_TCarTag
{
    UInt32 state;   //  运行状态 

    Int32 spin_diff_angle_;  // 逆时针方向为正

    Int32 motive_distance_;
    Int32 motive_angle_;
    
    Bot_TCarPlace last_place_;
    Bot_TCarPlace curr_place_;
    Bot_TCarPlace next_place_;

    PChar error_info_;      ///< 错误信息


    Bot_TCarDoStateNotifyMethod DoStateNotify;
    Bot_TCarDoGetCurrPlaceMethod DoGetCurrPlace;
    Bot_TCarDoSetCurrPlaceMethod DoSetCurrPlace;
    Bot_TCarDoSetMotivePwrMethod DoSetMotivePwr;
    Bot_TCarDoSetSpinPwrMethod  DoSetSpinPwr;
    Bot_TCarDoRunOnceMethod DoRunOnce;
    Bot_TCarDoCheckPlaceValidMethod DoCheckPlaceValid; 
};


Int32 Bot_TCarCreate(Bot_TCar *self);

void Bot_TCarDestroy(Bot_TCar *self);

void Bot_TCarRunOnce(Bot_TCar *self);


UInt32 Bot_TCarState(Bot_TCar *self);
Bool Bot_TCarSetPlaceMod(Bot_TCar *self, Bool is_place_mode);

Bool Bot_TCarSetMotivePwr(Bot_TCar *self, Int32 angle, Int32 distance);
Bool Bot_TCarSetSpinPwr(Bot_TCar *self, Int32 diff_angle);

Bool Bot_TCarSetPlace(Bot_TCar *self, const Bot_TCarPlace *place, Bool is_next_place);
Bool Bot_TCarGetPlace(Bot_TCar *self, Bot_TCarPlace *r_place, Bool is_next_place);

PChar Bot_TCarGetErrorInfo(Bot_TCar *self);

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _BOT_CAR_H_

/**
 * @}  Generated on "2024-04-02 21:49:29" by the tool "gen_hq_file.py >> V20231119" 
 */

