/**
 * @Unit    : 虚拟数据总线
 * @Version : 1.0
 * @Author  : 靳普诏
 * @Brief   : 简单介绍
 */


#ifndef _MDW_VIR_BUS_H_
#define _MDW_VIR_BUS_H_

#include "abc_types.h"


#ifdef __cplusplus
extern "C"
{
#endif



enum Mdw_TVirBusStateTag
{
    Mdw_kVirBusStateInvalid = 0,
    Mdw_kVirBusStateDisconnect = 1,
    Mdw_kVirBusStateConnecting = 2,
    Mdw_kVirBusStateTimeout = 3,
    Mdw_kVirBusStateConnected = 4,
};
typedef enum Mdw_TVirBusStateTag Mdw_TVirBusState;


typedef struct Mdw_IVirBusTag Mdw_IVirBus;

typedef Mdw_TVirBusState (*Mdw_IVirBusDoConnectMethod)(Mdw_IVirBus *self, const Byte *host, Int32 port);

typedef void (*Mdw_IVirBusDoDisconnectMethod)(Mdw_IVirBus *self);

typedef Int32 (*Mdw_IVirBusDoReadMethod)(Mdw_IVirBus *self, Byte *buf, Int32 buf_size);

typedef Int32 (*Mdw_IVirBusDoWriteMethod)(Mdw_IVirBus *self, const Byte *buf, Int32 buf_size);

typedef Mdw_TVirBusState (*Mdw_IVirBusDoGetState)(Mdw_IVirBus *self, PChar *r_info);

struct Mdw_IVirBusTag
{
    Mdw_IVirBusDoConnectMethod      DoConnect;
    Mdw_IVirBusDoDisconnectMethod   DoDisconnect;
    Mdw_IVirBusDoReadMethod         DoRead;
    Mdw_IVirBusDoWriteMethod        DoWrite;
    Mdw_IVirBusDoGetState           DoGetState;
};



void Mdw_IVirBusCreate(Mdw_IVirBus *self);

void Mdw_IVirBusDestroy(Mdw_IVirBus *self);

Bool Mdw_IVirBusIsConnected(Mdw_IVirBus *self);

Mdw_TVirBusState Mdw_IVirBusConnect(Mdw_IVirBus *self, const Byte *host, Int32 port);

void Mdw_IVirBusDisconnect(Mdw_IVirBus *self);

Int32 Mdw_IVirBusRead(Mdw_IVirBus *self, Byte *buf, Int32 buf_size);

Int32 Mdw_IVirBusWrite(Mdw_IVirBus *self, const Byte *buf, Int32 buf_size);

Mdw_TVirBusState Mdw_IVirBusGetState(Mdw_IVirBus *self, PChar *r_info);


#ifdef __cplusplus
}  // extern "C"
#endif

#endif



