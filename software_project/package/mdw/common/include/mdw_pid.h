/**
 * @addtogroup mdw_pid
 * mdw_pid
 * @{
 */
/**
 * @file        mdw_pid.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/06
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/06   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */

#ifndef _MDW_PID_H_
#define _MDW_PID_H_

#include "abc_types.h"

#ifdef __cplusplus
extern "C"
{
#endif


typedef struct Mdw_TPidTag Mdw_TPid;
struct Mdw_TPidTag
{
    Int64 kp;
    Int64 ki;
    Int64 kd;

    Int64 max_out;
    Int64 min_out;

    Int64 max_err;
    Int64 min_err;
    
    Int64 max_err_diff;
    Int64 min_err_diff;
    Int64 max_err_sum;
    Int64 min_err_sum;
    
    Int64 err_sum_;
    Int64 last_err_;
};


Int32 Mdw_TPidCreate(Mdw_TPid *self, Int64 kp, Int64 ki, Int64 kd, Int64 max_out, Int64 min_out);


void Mdw_TPidDestroy(Mdw_TPid *self);


/**
 * 重置误差积分项
 * @param *self
 * @return 
 */
Int32 Mdw_TPidReset(Mdw_TPid *self);


/**
 * 计算本次输出量
 * @param *self 对象句柄
 * @param curr_err 当前误差 = 期望值 - 当前值
 * @return 本次输出值
 */
Int64 Mdw_TPidCalc(Mdw_TPid *self, Int64 curr_err);





#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _MDW_PID_H_

/**
 * @}  Generated on "2024-04-06 18:44:01" by the tool "gen_hq_file.py >> V20231119" 
 */

