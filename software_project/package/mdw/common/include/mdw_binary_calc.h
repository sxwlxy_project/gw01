/**
 * @Unit    : 二值化计算器
 * @Version : 1.0
 * @Author  : 靳普诏
 * @Brief   : 简单介绍
 */


#ifndef _MDW_BINARY_CALC_H_
#define _MDW_BINARY_CALC_H_

#include "abc_types.h"


#ifdef __cplusplus
extern "C"
{
#endif



enum MDW_TBinaryValTag
{
    Mdw_kBinaryValInvalid   = -1,
    Mdw_kBinaryValZero      = 0,
    Mdw_kBinaryValOne       = 1,
};
typedef enum MDW_TBinaryValTag MDW_TBinaryVal;




typedef struct MDW_TBinaryCalcTag MDW_TBinaryCalc;


typedef Int32 (* MDW_TBinaryCalcUpdataSourceValMethod)(MDW_TBinaryCalc *self, void *user_param);

struct MDW_TBinaryCalcTag
{
    UInt8 id;
    Int8  binary_val_;
    Int32 source_val_;
    Int32 rising_thershold_;
    Int32 falling_thershold_;
    
    void *user_param_;
    MDW_TBinaryCalcUpdataSourceValMethod DoUpdataSourceVal_;
};


/* ~~~~~~~~~~~~~~~ 创建/析构 ~~~~~~~~~~~~~~~~ */

/**
 * 初始化  
 * @param *self                 二值化计算器
 * @param inital_val            初始值（@ref MDW_TBinaryVal）
 * @param rising_thershold      上升沿判断阈值
 * @param falling_thershold     下降沿判断阈值    
 * @return  >=  0   成功；
 *          <   0   失败，上升沿阈值 < 下降沿阈值
 */
Int32 MDW_TBinaryCalcCreate(MDW_TBinaryCalc *self, Int8 inital_val, Int32 rising_thershold, Int32 falling_thershold);


/**
 * 析构  
 * @param *self         二值化计算器
 * @return 
 */
void MDW_TBinaryCalcDestroy(MDW_TBinaryCalc *self);


/* ~~~~~~~~~~~~~~~ 属性方法 ~~~~~~~~~~~~~~~~~ */

/**
 * 读取上升沿阈值
 * @param *self     二值化计算器
 * @return  上升沿阈值
 */
Int32 MDW_TBinaryCalcRisingThershold(MDW_TBinaryCalc *self);

/**
 * 设置上升沿阈值 
 * @param *self             二值化计算器
 * @param thershold_val     阈值
 * @return  == thershold_val 成功；
 *          != thershold_val 失败；
 */
Int32 MDW_TBinaryCalcSetRisingThershold(MDW_TBinaryCalc *self, Int32 thershold_val);

/**
 * 读取下降沿阈值
 * @param *self     二值化计算器
 * @return  下降沿阈值
 */
Int32 MDW_TBinaryCalcFallingThershold(MDW_TBinaryCalc *self);

/**
 * 设置下降沿阈值 
 * @param *self             二值化计算器
 * @param thershold_val     阈值
 * @return  == thershold_val 成功；
 *          != thershold_val 失败；
 */
Int32 MDW_TBinaryCalcSetFallingThershold(MDW_TBinaryCalc *self, Int32 thershold_val);

/**
 * 设置上升/下降沿阈值
 * @param *self             二值化计算器
 * @param rising_thershold  上升沿阈值 
 * @param falling_thershold 下降沿阈值
 * @return  True 成功
 *          False 失败
 */
Bool MDW_TBinaryCalcSetThersholds(MDW_TBinaryCalc *self, Int32 rising_thershold, Int32 falling_thershold);

/**
 * 读取当前二值化值
 * @param *self
 * @return binary_val
 */
Int8 MDW_TBinaryCalcGetBinaryVal(MDW_TBinaryCalc *self);

/**
 * 读取当前内部原始值
 * @param *self
 * @return binary_val
 */
Int32 MDW_TBinaryCalcGetSourceVal(MDW_TBinaryCalc *self);



Bool MDW_TBinaryCalcSetUpdataSourceValMethod(MDW_TBinaryCalc *self, MDW_TBinaryCalcUpdataSourceValMethod method, void *user_param);

Int32 MDW_TBinaryCalcSetId(MDW_TBinaryCalc *self, UInt8 id);

/* ~~~~~~~~~~~~~~~ 公有方法 ~~~~~~~~~~~~~~~~~ */


/**
 * 更新当前 binary_val
 * @param *self             二值化计算器
 * @param curr_calc_val     当前需要计算的值
 * @return binary_val
 */
MDW_TBinaryVal MDW_TBinaryCalcUpdateBinaryVal(MDW_TBinaryCalc *self);





#ifdef __cplusplus
}  // extern "C"
#endif

#endif



