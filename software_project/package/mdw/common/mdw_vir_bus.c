#include "mdw_vir_bus.h"
#include <string.h>

void Mdw_IVirBusCreate(Mdw_IVirBus *self)
{
    if (self != NULL)
    {
        memset(self, 0, sizeof(*self));
    }
}

void Mdw_IVirBusDestroy(Mdw_IVirBus *self)
{
    if (self != NULL)
    {
        if (Mdw_IVirBusIsConnected(self))
        {
            Mdw_IVirBusDisconnect(self);
        }
        
        memset(self, 0, sizeof(*self));
    }
}


Mdw_TVirBusState Mdw_IVirBusConnect(Mdw_IVirBus *self, const Byte *host, Int32 port)
{
    Mdw_TVirBusState result = Mdw_kVirBusStateInvalid;

    if (self->DoConnect != NULL)
    {
        result = self->DoConnect(self, host, port);
    }

    return result;
}

Bool Mdw_IVirBusIsConnected(Mdw_IVirBus *self)
{
    return Mdw_IVirBusGetState(self, NULL) == Mdw_kVirBusStateConnected;
}

void Mdw_IVirBusDisconnect(Mdw_IVirBus *self)
{
    if (self->DoDisconnect != NULL)
    {
        self->DoDisconnect(self);
    }
}

Int32 Mdw_IVirBusRead(Mdw_IVirBus *self, Byte *buf, Int32 buf_size)
{
    Int32 result = -404;

    if (self->DoRead != NULL)
    {
        result = self->DoRead(self, buf, buf_size);
    }

    return result;
}

Int32 Mdw_IVirBusWrite(Mdw_IVirBus *self, const Byte *buf, Int32 buf_size)
{
    Int32 result = -404;

    if (self->DoWrite != NULL)
    {
        result = self->DoWrite(self, buf, buf_size);
    }

    return result;
}


Mdw_TVirBusState Mdw_IVirBusGetState(Mdw_IVirBus *self, PChar *r_info)
{
    Mdw_TVirBusState result = Mdw_kVirBusStateInvalid;
    
    if (self->DoGetState != NULL)
        result = self->DoGetState(self, r_info);

    return result;
}
