/**
 * @addtogroup rte_bot_car
 * rte_bot_car
 * @{
 */
/**
 * @file        rte_bot_car.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏
 * @date        2024/04/13
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/13   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 
 */

#ifndef _RTE_BOT_CAR_H_
#define _RTE_BOT_CAR_H_

#include "abc_types.h"
#include "bot_car.h"
#include "mdw_pid.h"
#include "bsp_axis_def.h"
#include "rte_bot_car_place.h"

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct Rte_TBotCarTag Rte_TBotCar;
struct Rte_TBotCarTag
{
    Bot_TCar parent_;
    
    Bsp_THandle moto_front_left_;
    Bsp_THandle moto_front_right_;
    Bsp_THandle moto_rear_left_;
    Bsp_THandle moto_rear_right_;
    
    Int32 spin_pwr_;
    Int32 motive_x_pwr_;
    Int32 motive_y_pwr_;
    
    Int32 moto_front_left_pwr_;
    Int32 moto_front_right_pwr_;
    Int32 moto_rear_left_pwr_;
    Int32 moto_rear_right_pwr_;
    
    Mdw_TPid spin_pid_;
    
    Bsp_THandle axis_;
    Int32 yaw_offset;
    Bsp_TAxisAngleData axis_data_buf_[3];
    Bsp_TAxisAngleData axis_data_array_[1];
    
    Bsp_THandle laser_;
    Bsp_TLaserData laser_data_buf_[3];
    Bsp_TLaserData laser_data_array_[1];
    Rte_TBotCarPlace place_calc_;
};


void Rte_BotCarCreate(void);

void Rte_BotCarInit(void);

Int32 Rte_BotCarRunOnce(void);

void Rte_BotCarDone(void);

void Rte_BotCarDestroy(void);


Bot_TCar *Rte_BotCarHandle(void);

#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _RTE_BOT_CAR_H_

/**
 * @}  Generated on "2024-04-13 11:03:07" by the tool "gen_hq_file.py >> V20231119" 
 */

