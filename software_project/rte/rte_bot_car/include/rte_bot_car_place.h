/**
 * @addtogroup rte_bot_car_place
 * rte_bot_car_place
 * @{
 */
/**
 * @file        rte_bot_car_place.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/21
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/21   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */

#ifndef _RTE_BOT_CAR_PLACE_H_
#define _RTE_BOT_CAR_PLACE_H_

#include "abc_types.h"
#include "bot_car.h"
#include "bsp_laser.h"
#include "bsp_laser_def.h"


#define PLAT_X_MAX (3100)  // 地图 x方向 最大距离
#define PLAT_Y_MAX (2600)  // 地图 x方向 最大距离

#define CAB_X_OFFSEET_LONG (1800)  // 货柜x方向 
#define CAB_X_OFFSEET_SHORT (500)  // 货柜x方向 
#define CAB_Y_OFFSEET_LONG (1800)  // 货柜y方向 
#define CAB_Y_OFFSEET_SHORT (0)  // 货柜y方向 

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct Rte_TBotCarPlaceTag Rte_TBotCarPlace;
struct Rte_TBotCarPlaceTag
{
    void *owner;       // 实际类型为 (Rte_TBotCar *)

    Int8 laser_xf_idx;  // 前激光数据源编号
    Int8 laser_xb_idx;  // 后激光数据源编号
    Int8 laser_yf_idx;  // 左激光数据源编号
    Int8 laser_yb_idx;  // 右激光数据源编号

    Int32 laser_offset_[4];  // 激光数据源偏移量. 将激光数据转换成车体中心到反射点距离
    Int32 laser_orig_data_[4];  // 原始激光数据

    Int32 place_calc_x_;
    Int32 place_calc_y_;

    Int8 flag_x_offset;    // 0 不做补偿；1 正方向数据补偿长值 负方向补偿短； 2 负方向数据补偿长值 正方向补偿短
    Int8 flag_y_offset;    // 0 不做补偿；1 正方向数据补偿； 2 负方向数据补偿
    Int8 state_;    // 0 初始化状态； 1 运行状态；2 结束状态
    UInt32 updata_cnt_;
};



Int32 Rte_TBotCarPlaceCreate(Rte_TBotCarPlace *self);

Bool Rte_TBotCarPlaceInit(Rte_TBotCarPlace *self);

void Rte_TBotCarPlaceRunOnce(Rte_TBotCarPlace *self);

Bool Rte_TBotCarPlaceUpdata(Rte_TBotCarPlace *self, Int32 orig_data[4]);

Bool Rte_TBotCarPlaceGetPlace(Rte_TBotCarPlace *self, Int32 *x, Int32 *y);

void Rte_TBotCarPlaceDone(Rte_TBotCarPlace *self);

void Rte_TBotCarPlaceDestroy(Rte_TBotCarPlace *self);




#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _RTE_BOT_CAR_PLACE_H_

/**
 * @}  Generated on "2024-04-21 12:00:44" by the tool "gen_hq_file.py >> V20231119" 
 */

