/**
 * @addtogroup rte_task
 * rte_task
 * @{
 */
/**
 * @file        rte_task.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(499367619@qq.com)
 * @date        2024/04/13
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/13   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 
 */

#ifndef _RTE_TASK_H_
#define _RTE_TASK_H_

#include "rte_task_config.h"
#include "osc_task.h"


#ifdef __cplusplus
extern "C"
{
#endif

enum Rte_TaskIdTag
{
    Rte_kTaskIdMain = 0,
    Rte_kTaskIdCar = 1,
    Rte_kTaskIdGrab = 2,
    Rte_kTaskIdRaspPi = 3,
};


Osc_TTask *Rte_CreateTask(enum Rte_TaskIdTag taskId);
void Rte_DestroyTask(enum Rte_TaskIdTag taskId);



#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _RTE_TASK_H_

/**
 * @}  Generated on "2024-04-13 01:23:06" by the tool "gen_hq_file.py >> V20231119" 
 */

