/**
 * @addtogroup rte_task_config
 * rte_task_config
 * @{
 */
/**
 * @file        rte_task_config.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏
 * @date        2024/04/13
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/13   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 
 */

#ifndef _RTE_TASK_CONFIG_H_
#define _RTE_TASK_CONFIG_H_

#include "abc_types.h"
#include "osc_task.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define RTE_TASK_MAIN_NAME        "Main"
#define RTE_TASK_MAIN_PRIORITY    Osc_kTaskPriorityNormal
#define RTE_TASK_MAIN_STACK_SIZE  (1024 * 1)

#define RTE_TASK_CAR_NAME         "Car"
#define RTE_TASK_CAR_PRIORITY     Osc_kTaskPriorityNormal
#define RTE_TASK_CAR_STACK_SIZE   (1024 * 1)

#define RTE_TASK_GRAB_NAME      "Grab"
#define RTE_TASK_GRAB_PRIORITY    Osc_kTaskPriorityNormal
#define RTE_TASK_GRAB_STACK_SIZE  (1024 * 1)

#define RTE_TASK_RASP_PI_NAME     "RespPi"
#define RTE_TASK_RASP_PI_PRIORITY  Osc_kTaskPriorityNormal
#define RTE_TASK_RASP_PI_STACK_SIZE  (1024 * 1)



#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _RTE_TASK_CONFIG_H_

/**
 * @}  Generated on "2024-04-13 01:31:49" by the tool "gen_hq_file.py >> V20231119" 
 */

