/**
 * @file        rte_task.c
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏
 * @date        2024/04/13
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/13   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 
 */
 
#include "rte_task.h"


static Osc_TTask g_task_main_ = {0};
static Int8 g_task_main_stack_mem_[RTE_TASK_MAIN_STACK_SIZE] __attribute__((aligned(8))) = {0};
static const Osc_TTaskAttr g_task_main_attr_ = {
    .name = RTE_TASK_MAIN_NAME,
    .priority = RTE_TASK_MAIN_PRIORITY,
    .stack_mem = g_task_main_stack_mem_,
    .stack_size = sizeof(g_task_main_stack_mem_),
};


static Osc_TTask g_task_car_ = {0};
static Int8 g_task_car_stack_mem_[RTE_TASK_CAR_STACK_SIZE] __attribute__((aligned(8))) = {0};
static const Osc_TTaskAttr g_task_car_attr_ = {
    .name = RTE_TASK_CAR_NAME,
    .priority = RTE_TASK_CAR_PRIORITY,
    .stack_mem = g_task_car_stack_mem_,
    .stack_size = sizeof(g_task_car_stack_mem_),
};


static Osc_TTask g_task_grab_ = {0};
static Int8 g_task_grab_stack_mem_[RTE_TASK_GRAB_STACK_SIZE] __attribute__((aligned(8))) = {0};
static const Osc_TTaskAttr g_task_grab_attr_ = {
    .name = RTE_TASK_GRAB_NAME,
    .priority = RTE_TASK_GRAB_PRIORITY,
    .stack_mem = g_task_grab_stack_mem_,
    .stack_size = sizeof(g_task_grab_stack_mem_),
};


static Osc_TTask g_task_rasp_pi_ = {0};
static Int8 g_task_rasp_pi_stack_mem_[RTE_TASK_RASP_PI_STACK_SIZE] __attribute__((aligned(8))) = {0};
static const Osc_TTaskAttr g_task_rasp_pi_attr_ = {
    .name = RTE_TASK_RASP_PI_NAME,
    .priority = RTE_TASK_RASP_PI_PRIORITY,
    .stack_mem = g_task_rasp_pi_stack_mem_,
    .stack_size = sizeof(g_task_rasp_pi_stack_mem_),
};





Osc_TTask *Rte_CreateTask(enum Rte_TaskIdTag task_id)
{
    Osc_TTask *task = NULL;
    const Osc_TTaskAttr *attr = NULL;
    Int32 error = 0;

    switch (task_id)
    {
        case Rte_kTaskIdMain:
        {
            task = &g_task_main_;
            attr = &g_task_main_attr_;
            break;
        }

        case Rte_kTaskIdCar:
        {
            task = &g_task_car_;
            attr = &g_task_car_attr_;
            break;
        }

        case Rte_kTaskIdGrab:
        {
            task = &g_task_grab_;
            attr = &g_task_grab_attr_;
            break;
        }

        case Rte_kTaskIdRaspPi:
        {
            task = &g_task_rasp_pi_;
            attr = &g_task_rasp_pi_attr_;
            break;
        }

        default:
        {
            error = -1;
            break;
        }
    }

    if (error == 0)
    {
        error = Osc_TTaskCreate(task, attr);
        
        if (error < 0)
        {
            task = NULL;
            error = -2;
        }

    }

    return task;
}


void Rte_DestroyTask(enum Rte_TaskIdTag task_id)
{
    Osc_TTask *task = NULL;

    switch (task_id)
    {
        case Rte_kTaskIdMain:
        {
            task = &g_task_main_;
            break;
        }

        case Rte_kTaskIdCar:
        {
            task = &g_task_car_;
            break;
        }

        case Rte_kTaskIdGrab:
        {
            task = &g_task_grab_;
            break;
        }

        case Rte_kTaskIdRaspPi:
        {
            task = &g_task_rasp_pi_;
            break;
        }

        default:
        {
            break;
        }
        
    }

    Osc_TTaskDestroy(task);
}


///< Generated on "2024-04-13 01:23:06" by the tool "gen_hq_file.py >> V20231119" 

