/**
 * @addtogroup rte_bsc_shell
 * rte_bsc_shell
 * @{
 */
/**
 * @file        rte_bsc_shell.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/06
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/06   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */

#ifndef _RTE_BSC_SHELL_H_
#define _RTE_BSC_SHELL_H_


#ifdef __cplusplus
extern "C"
{
#endif


void Rte_Bsc_ShellCreate(void);
void Rte_Bsc_ShellInit(void);

void Rte_Bsc_ShellRunOnce(void);

void Rte_Bsc_ShellDone(void);
void Rte_Bsc_ShellDestroy(void);





#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _RTE_BSC_SHELL_H_

/**
 * @}  Generated on "2024-04-06 21:20:42" by the tool "gen_hq_file.py >> V20231119" 
 */

