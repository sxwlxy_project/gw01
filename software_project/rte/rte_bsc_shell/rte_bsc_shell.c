/**
 * @file        rte_bsc_shell.c
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/06
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/06   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */
 
#include "rte_bsc_shell.h"

#include "bsp_def.h"
#include "bsp_inc.h"

static Bsp_THandle g_uart0_handle_ = NULL;
static Int8 g_uart0_tx_buf_[256] = {0};
static Int8 g_uart0_rx_buf_[256] = {0};








// ------------------------------------------------------

void Rte_Bsc_ShellCreate(void)
{
    g_uart0_handle_ = Bsp_UartCreate(Bsp_kUartId0, g_uart0_tx_buf_, sizeof(g_uart0_tx_buf_), g_uart0_rx_buf_, sizeof(g_uart0_rx_buf_));

}

void Rte_Bsc_ShellInit(void)
{
    Bsp_UartOpen(g_uart0_handle_, 115200, Bsp_kUartParityNone);
}


void Rte_Bsc_ShellRunOnce(void)
{
    Int8 try_cnt = 0;
    Int8 cmd[256] = {0};
    Int32 read_size = Bsp_UartRead(g_uart0_handle_, cmd, sizeof(cmd));

    while (read_size > 0)
    {
        Bsp_UartWrite(g_uart0_handle_, cmd, read_size);
    
        if (try_cnt >= 3)
            break;
        
        try_cnt++;
        read_size = Bsp_UartRead(g_uart0_handle_, cmd, read_size);
    }
}


void Rte_Bsc_ShellDone(void)
{
    Bsp_UartClose(g_uart0_handle_);
}

void Rte_Bsc_ShellDestroy(void)
{
    Bsp_UartFree(g_uart0_handle_);
}





///< Generated on "2024-04-06 21:20:42" by the tool "gen_hq_file.py >> V20231119" 

