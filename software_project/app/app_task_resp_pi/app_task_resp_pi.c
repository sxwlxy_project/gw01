/**
 * @file        app_task_resp_pi.c
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/13
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/13   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */
 
#include "app_task_resp_pi.h"
#include "rte_task.h"


typedef struct App_TTaskRespPiTag App_TTaskRespPi;
struct App_TTaskRespPiTag
{
    Osc_TTask *task_handle_;
};

static App_TTaskRespPi g_app_task_resp_pi_ = {0};


// -----------------------------------------------------------------

static void L_App_TaskRespPiOnStart(Osc_TTask *sender)
{
    // ... todo ...
}

static UInt32 L_App_TaskRespPiOnRunOnce(Osc_TTask *self)
{
    UInt32 result = 50;
    
    // ... todo ...
    return result;
}


static void L_App_TaskRespPiOnTerminate(Osc_TTask *self)
{
    // ... todo ...
}


// -----------------------------------------------------------------

void App_TaskRespPiCreate(void)
{
    g_app_task_resp_pi_.task_handle_ = Rte_CreateTask(Rte_kTaskIdRaspPi);
}

void App_TaskRespPiInit(void)
{   
    Osc_TTask *task_handle = g_app_task_resp_pi_.task_handle_;
    
    if (task_handle)
    {
        g_app_task_resp_pi_.task_handle_->OnStart = L_App_TaskRespPiOnStart;
        g_app_task_resp_pi_.task_handle_->OnRunOnce = L_App_TaskRespPiOnRunOnce;
        g_app_task_resp_pi_.task_handle_->OnTerminate = L_App_TaskRespPiOnTerminate;
    }
}

void App_TaskRespPiStart(void)
{
    Osc_TTask *task_handle = g_app_task_resp_pi_.task_handle_;
    
    if (task_handle)
    {
        Osc_TTaskStart(task_handle);
    }
}

void App_TaskRespPiStop(void)
{
    Osc_TTask *task_handle = g_app_task_resp_pi_.task_handle_;

    if (task_handle)
    {
        Osc_TTaskTerminate(task_handle);
    }
}

void App_TaskRespPiDone(void)
{
 
}

void App_TaskRespPiDestroy(void)
{
   
}








///< Generated on "2024-04-13 10:55:48" by the tool "gen_hq_file.py >> V20231119" 

