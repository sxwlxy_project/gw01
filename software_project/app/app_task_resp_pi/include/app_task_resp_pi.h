/**
 * @addtogroup app_task_resp_pi
 * app_task_resp_pi
 * @{
 */
/**
 * @file        app_task_resp_pi.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(499367619@qq.com)
 * @date        2024/04/13
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/13   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 
 */

#ifndef _APP_TASK_RESP_PI_H_
#define _APP_TASK_RESP_PI_H_


#ifdef __cplusplus
extern "C"
{
#endif


void App_TaskRespPiCreate(void);

void App_TaskRespPiInit(void);

void App_TaskRespPiStart(void);

void App_TaskRespPiStop(void);

void App_TaskRespPiDone(void);

void App_TaskRespPiDestroy(void);


#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _APP_TASK_RESP_PI_H_

/**
 * @}  Generated on "2024-04-13 10:55:48" by the tool "gen_hq_file.py >> V20231119" 
 */

