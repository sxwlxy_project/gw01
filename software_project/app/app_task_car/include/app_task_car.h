/**
 * @addtogroup app_task_car
 * app_task_car
 * @{
 */
/**
 * @file        app_task_car.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(499367619@qq.com)
 * @date        2024/04/13
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/13   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 
 */

#ifndef _APP_TASK_CAR_H_
#define _APP_TASK_CAR_H_


#ifdef __cplusplus
extern "C"
{
#endif


void App_TaskCarCreate(void);

void App_TaskCarInit(void);

void App_TaskCarStart(void);

void App_TaskCarStop(void);

void App_TaskCarDone(void);

void App_TaskCarDestroy(void);


#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _APP_TASK_CAR_H_

/**
 * @}  Generated on "2024-04-13 10:55:48" by the tool "gen_hq_file.py >> V20231119" 
 */

