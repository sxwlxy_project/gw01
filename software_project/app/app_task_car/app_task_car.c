/**
 * @file        app_task_car.c
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/13
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/13   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */
 
#include "app_task_car.h"
#include "rte_task.h"
#include "rte_bot_car.h"

typedef struct App_TTaskCarTag App_TTaskCar;
struct App_TTaskCarTag
{
    Osc_TTask *task_handle_;
};

static App_TTaskCar g_app_task_car_ = {0};


// -----------------------------------------------------------------

static void L_App_TaskCarOnStart(Osc_TTask *sender)
{
    Rte_BotCarCreate();
    Rte_BotCarInit();
}

static UInt32 L_App_TaskCarOnRunOnce(Osc_TTask *self)
{
    UInt32 result = 0;
    
    // ... todo ...
    result = Rte_BotCarRunOnce();
    
    return result;
}


static void L_App_TaskCarOnTerminate(Osc_TTask *self)
{
    Rte_BotCarDone();
    Rte_BotCarDestroy();
}


// -----------------------------------------------------------------

void App_TaskCarCreate(void)
{
    g_app_task_car_.task_handle_ = Rte_CreateTask(Rte_kTaskIdCar);
    
}

void App_TaskCarInit(void)
{   
    Osc_TTask *task_handle = g_app_task_car_.task_handle_;
    
    if (task_handle)
    {
        g_app_task_car_.task_handle_->OnStart = L_App_TaskCarOnStart;
        g_app_task_car_.task_handle_->OnRunOnce = L_App_TaskCarOnRunOnce;
        g_app_task_car_.task_handle_->OnTerminate = L_App_TaskCarOnTerminate;
    }
    
    
}

void App_TaskCarStart(void)
{
    Osc_TTask *task_handle = g_app_task_car_.task_handle_;
    
    if (task_handle)
    {
        Osc_TTaskStart(task_handle);
    }
}

void App_TaskCarStop(void)
{
    Osc_TTask *task_handle = g_app_task_car_.task_handle_;

    if (task_handle)
    {
        Osc_TTaskTerminate(task_handle);
    }
}

void App_TaskCarDone(void)
{
    
}

void App_TaskCarDestroy(void)
{
    
}








///< Generated on "2024-04-13 10:55:48" by the tool "gen_hq_file.py >> V20231119" 

