/**
 * @file        gw01_car_main.c
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/06
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/06   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */
 
#include "gw01_car_main.h"
#include "osc_application.h"
#include "osc_api.h"

//#include "rte_bsc_shell.h"
#include "rte_task.h"

#include "bsp_base.h"
#include "bsp_led_def.h"
#include "bsp_led.h"

#include "app_task_car.h"
#include "app_task_grab.h"
#include "app_task_resp_pi.h"

#include "rte_bot_car.h"


static void L_MainTaskOnStart(Osc_TTask *self)
{
    App_TaskCarCreate();
    App_TaskCarInit();
    App_TaskCarStart();
    
    App_TaskGrabCreate();
    App_TaskGrabInit();
    App_TaskGrabStart();
    
    App_TaskRespPiCreate();
    App_TaskRespPiInit();
    App_TaskRespPiStart();
}



static UInt32 L_MainTaskOnRunOnce(Osc_TTask *self)
{
    static UInt32 sleep = 0;
    static Bool add_flag = True;
    Bot_TCar *car_handle = Rte_BotCarHandle();    
    Bool status;
    
    Bsp_LedGetStatus(Bsp_kLedId0, &status);
    Bsp_LedSetOn(Bsp_kLedId0, !status);
    
    
//    
    if (car_handle == NULL)
        return 1000;
//    car_handle = Rte_BotCarHandle();
//    Bot_TCarSetMotivePwr(car_handle, 90 * 1000, 1000);
//    Osc_SleepMs(500);
////    
//    car_handle = Rte_BotCarHandle();
//    Bot_TCarSetMotivePwr(car_handle, 90* 1000, -1000);
//    Osc_SleepMs(500);

    Bot_TCarPlace place; 

    Bot_TCarGetPlace(car_handle, &place, False);
    place.x += 100;
    Bot_TCarSetPlace(car_handle, &place, True);
    while ((Bot_TCarState(car_handle) & Bot_kCarStateMaskArrived) == 0) 
    {
        Osc_SleepMs(100);
    };

    Bot_TCarGetPlace(car_handle, &place, False);
    place.x += 200;
    Bot_TCarSetPlace(car_handle, &place, True);
    while ((Bot_TCarState(car_handle) & Bot_kCarStateMaskArrived) == 0) 
    {
        Osc_SleepMs(100);
    };

    Bot_TCarGetPlace(car_handle, &place, False);
    place.x -= 100;
    Bot_TCarSetPlace(car_handle, &place, True);
    while ((Bot_TCarState(car_handle) & Bot_kCarStateMaskArrived) == 0) 
    {
        Osc_SleepMs(100);
    };
    
    Bot_TCarGetPlace(car_handle, &place, False);
    place.x -= 200;
    Bot_TCarSetPlace(car_handle, &place, True);
    while ((Bot_TCarState(car_handle) & Bot_kCarStateMaskArrived) == 0) 
    {
        Osc_SleepMs(100);
    };

    return sleep;
}


static void L_MainTaskOnTerminate(Osc_TTask *self)
{
    App_TaskCarStop();
    App_TaskCarDone();
    App_TaskCarDestroy();
    
    App_TaskGrabStop();
    App_TaskGrabDone();
    App_TaskGrabDestroy();
    
    App_TaskRespPiStop();
    App_TaskRespPiDone();
    App_TaskRespPiDestroy();
}

int main(void)
{
    Bsp_Init();
    Bsp_Open();
    
    Osc_TApplicationInitialize(&g_osc_application);
    
    {
        Osc_TTask *main_task = Rte_CreateTask(Rte_kTaskIdMain);

        main_task->OnStart = L_MainTaskOnStart;
        main_task->OnRunOnce = L_MainTaskOnRunOnce;
        main_task->OnTerminate = L_MainTaskOnTerminate;

        Osc_TApplicationSetMainTask(&g_osc_application, main_task);
    }
    
    Osc_TApplicationRun(&g_osc_application);
    
    Bsp_Close();
    Bsp_Done();
    
    return 0;
}


///< Generated on "2024-04-06 17:07:11" by the tool "gen_hq_file.py >> V20231119" 

