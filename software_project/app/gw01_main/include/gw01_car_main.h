/**
 * @addtogroup gw01_car_main
 * gw01_car_main
 * @{
 */
/**
 * @file        gw01_car_main.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/06
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/06   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */

#ifndef _GW01_CAR_MAIN_H_
#define _GW01_CAR_MAIN_H_



#ifdef __cplusplus
extern "C"
{
#endif



//void GW01_Main(void);


#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _GW01_CAR_MAIN_H_

/**
 * @}  Generated on "2024-04-06 17:07:11" by the tool "gen_hq_file.py >> V20231119" 
 */

