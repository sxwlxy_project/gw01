/**
 * @addtogroup app_task_grab
 * app_task_grab
 * @{
 */
/**
 * @file        app_task_grab.h
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(499367619@qq.com)
 * @date        2024/04/13
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/13   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 
 */

#ifndef _APP_TASK_GRAB_H_
#define _APP_TASK_GRAB_H_


#ifdef __cplusplus
extern "C"
{
#endif


void App_TaskGrabCreate(void);

void App_TaskGrabInit(void);

void App_TaskGrabStart(void);

void App_TaskGrabStop(void);

void App_TaskGrabDone(void);

void App_TaskGrabDestroy(void);


#ifdef __cplusplus
}  ///< extern "C"
#endif

#endif  ///< _APP_TASK_GRAB_H_

/**
 * @}  Generated on "2024-04-13 10:55:48" by the tool "gen_hq_file.py >> V20231119" 
 */

