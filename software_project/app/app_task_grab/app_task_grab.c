/**
 * @file        app_task_grab.c
 * @brief       XXXX
 * @note        XXXX
 * @author      靳普诏(puzhao.jin@hopechart.com)
 * @date        2024/04/13
 * @version     1.0

 * @par         修改日志
 * <table>
 * <tr><th>Date         <th>Version     <th>Author      <th> Description
 * <tr><td>2024/04/13   <td>1.0         <td>靳普诏       <td> 创建初始版本
 * @copyright 杭州鸿泉物联网技术股份有限公司
 */
 
#include "app_task_grab.h"
#include "rte_task.h"


typedef struct App_TTaskGrabTag App_TTaskGrab;
struct App_TTaskGrabTag
{
    Osc_TTask *task_handle_;
};

static App_TTaskGrab g_app_task_grab_ = {0};


// -----------------------------------------------------------------

static void L_App_TaskGrabOnStart(Osc_TTask *sender)
{
    // ... todo ...
}

static UInt32 L_App_TaskGrabOnRunOnce(Osc_TTask *self)
{
    UInt32 result = 50;
    
    // ... todo ...
    return result;
}


static void L_App_TaskGrabOnTerminate(Osc_TTask *self)
{
    // ... todo ...
}


// -----------------------------------------------------------------

void App_TaskGrabCreate(void)
{
    g_app_task_grab_.task_handle_ = Rte_CreateTask(Rte_kTaskIdGrab);
}

void App_TaskGrabInit(void)
{   
    Osc_TTask *task_handle = g_app_task_grab_.task_handle_;
    
    if (task_handle)
    {
        g_app_task_grab_.task_handle_->OnStart = L_App_TaskGrabOnStart;
        g_app_task_grab_.task_handle_->OnRunOnce = L_App_TaskGrabOnRunOnce;
        g_app_task_grab_.task_handle_->OnTerminate = L_App_TaskGrabOnTerminate;
    }
}

void App_TaskGrabStart(void)
{
    Osc_TTask *task_handle = g_app_task_grab_.task_handle_;
    
    if (task_handle)
    {
        Osc_TTaskStart(task_handle);
    }
}

void App_TaskGrabStop(void)
{
    Osc_TTask *task_handle = g_app_task_grab_.task_handle_;

    if (task_handle)
    {
        Osc_TTaskTerminate(task_handle);
    }
}

void App_TaskGrabDone(void)
{
 
}

void App_TaskGrabDestroy(void)
{
   
}





///< Generated on "2024-04-13 10:55:48" by the tool "gen_hq_file.py >> V20231119" 

